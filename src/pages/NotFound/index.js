import { useRouter } from "next/router";
import React, { useEffect } from "react";

import Layout from "lib/common/Layout";

const NotFound = () => {
  const router = useRouter();

  useEffect(() => {
    router.push("/");
  }, [router]);

  return (
    <Layout title="Page Not Found" showPreloader>
      <div className="container">
        <h1>Page not found</h1>
      </div>
    </Layout>
  );
};

export default React.memo(NotFound);
