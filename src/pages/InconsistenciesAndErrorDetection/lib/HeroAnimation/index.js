import Css from "./styles.module.scss";
import React from "react";
import WebmAnimation from "lib/common/WebmAnimation";

const HeroAnimation = () => {
  return (
    <div className={Css.container}>
      <WebmAnimation
        className={Css.animation}
        webmUrl="/video/inconsistencies-and-error-detection-hero.webm"
        movUrl="/video/inconsistencies-and-error-detection-hero.mov"
        pngUrl="/images/inconsistencies-and-error-detection-hero.png" />
    </div>
  );
};

export default HeroAnimation;
