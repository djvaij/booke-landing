import CallToAction from "lib/common/CallToAction";
import Css from "./styles.module.scss";
import Feature from "lib/common/Feature";
import FormBlock from "lib/common/FormBlock";
import HeroAnimation from "./lib/HeroAnimation";
import Layout from "lib/common/Layout";
import MainCustomized from "lib/common/MainCustomized";
import React from "react";
import classNames from "classnames";

const featuresBackgrounds = {
  first: (
    <svg width="559" height="389" viewBox="0 0 559 389" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M191.038 4.79537C195.762 1.12619 201.881 -0.235809 207.715 1.08296L542.996 76.8655C554.151 79.3869 560.924 90.7321 557.851 101.748L484.477 364.746C482.114 373.215 474.496 379.148 465.706 379.365L89.9193 388.636C81.1405 388.852 73.2472 383.317 70.4601 374.989L1.50014 168.946C-1.19559 160.891 1.49007 152.013 8.19813 146.803L191.038 4.79537Z"
        fill="#EFF5FF" />
    </svg>
  ),
  second: (
    <svg width="602" height="383" viewBox="0 0 602 383" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M530.778 10.2767C526.698 3.43041 518.943 -0.34006 511.039 0.679741L17.6538 64.3353C6.38458 65.7892 -1.41107 76.328 0.496948 87.5293L35.9191 295.478C37.3071 303.627 43.5648 310.082 51.6661 311.722L401.393 382.535C408.849 384.044 416.517 381.193 421.175 375.178L597.655 147.276C602.69 140.775 603.232 131.855 599.023 124.792L530.778 10.2767Z"
        fill="#EFF5FF" />
    </svg>
  ),
  third: (
    <svg width="561" height="403" viewBox="0 0 561 403" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M14.6452 114.888L406.375 1.21569C412.782 -0.643626 419.695 0.8145 424.805 5.10337L553.09 112.763C558.666 117.442 561.259 124.782 559.861 131.926L520.932 330.756C519.284 339.173 512.453 345.602 503.953 346.738L89.3321 402.122C79.3684 403.453 69.9649 397.182 67.3647 387.472L0.899659 139.27C-1.91569 128.756 4.19269 117.922 14.6452 114.888Z"
        fill="#EFF5FF" />
    </svg>
  )
};

const InconsistenciesAndErrorDetection = () => {
  return (
    <Layout title="Inconsistencies and error detection" index>
      <MainCustomized
        title={"Inconsistencies and\n error detection"}
        description="Spot discrepancies in your categorization "
        customAnimationBlock={<HeroAnimation />}
        rightClass={classNames(Css.right, Css.right)}
        leftClass={classNames(Css.left, Css.left)} />
      <div className="container">
        <Feature
          className={Css.feature}
          video={{
            webmUrl: "/video/inconsistencies-and-error-detection-automatic-rules-find.webm",
            mpegUrl: "/video/inconsistencies-and-error-detection-automatic-rules-find.mp4"
          }}
          bgImage={featuresBackgrounds.first}
          bgPosition={{
            top: "-36px",
            left: "-33px"
          }}
          title={"Automatic rules find the\n inconsistencies for you"}
          description={`Enjoy an intelligent screening of transactions that
            instantly defines all your inconsistencies into categories.`} />
        <Feature
          reverse
          className={Css.feature}
          video={{
            webmUrl: "/video/inconsistencies-and-error-detection-fix-the-discrepancies.webm",
            mpegUrl: "/video/inconsistencies-and-error-detection-fix-the-discrepancies.mp4"
          }}
          bgImage={featuresBackgrounds.second}
          bgPosition={{
            top: "-41px",
            left: "-40px"
          }}
          title="Fix the discrepancies"
          description={`The most pleasant part of the Audit is to fixing the
            inconsistencies knowing how big of an impact it has on P&L.`} />
        <Feature
          className={Css.feature}
          video={{
            webmUrl: "/video/inconsistencies-and-error-detection-tell-system-which.webm",
            mpegUrl: "/video/inconsistencies-and-error-detection-tell-system-which.mp4"
          }}
          bgImage={featuresBackgrounds.third}
          bgPosition={{
            top: "-29px",
            left: "-49px"
          }}
          title={"Tell system which\n ones are correct"}
          description="Thumbs up if you done a great job and categorized correctly." />

      </div>
      <CallToAction />
      <FormBlock />
    </Layout>
  );
};

export default InconsistenciesAndErrorDetection;
