import React from "react";

import Layout from "lib/common/Layout";

import Content from "./Content";

const TermsAndConditions = () => {
  return (
    <Layout title="Terms and conditions">
      <div className="terms">
        <div className="container">
          <Content />
        </div>
      </div>
    </Layout>
  );
};

export default TermsAndConditions;
