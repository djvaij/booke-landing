import React from "react";

import CallToAction from "lib/common/CallToAction";
import Counters from "lib/common/Counters";
import Features from "lib/common/Features";
import FormBlock from "lib/common/FormBlock";
import Infographics from "lib/common/Infographics";
import Layout from "lib/common/Layout";
import MainAlt from "lib/common/MainAlt";
import Pricing from "lib/common/Pricing";
import VideoFeedback from "lib/common/VideoFeedback";
import useTexts from "hooks/useTexts";

const QuickbooksDesktop = () => {
  const texts = useTexts();

  return (
    <Layout
      index title={texts.qbDesktopPageTitle}
      description={texts.qbDesktopPageDescription}>
      <MainAlt
        title={texts.qbDesktopPageTitle}
        subtitle={texts.qbDesktopPageSubtitle}
        description={texts.qbDesktopPageDescription} />
      <Infographics />
      <Features />
      <VideoFeedback />
      <Counters />
      <Pricing />
      <CallToAction />
      <FormBlock />
    </Layout>
  );
};

export default QuickbooksDesktop;
