import React from "react";

const TYPE_DOC = "doc";

const TYPE_TEXT = "text";

const TYPE_PARAGRAPH = "paragraph";

const TYPE_HARD_BREAK = "hard_break";

const TYPE_BULLET_LIST = "bullet_list";

const TYPE_LIST_ITEM = "list_item";

const TYPE_HEADING = "heading";

const ORDERED_LIST = "ordered_list";

const LIST_ITEM = "list_item";

const BLOCKQUOTE = "blockquote";

const HORIZONTAL_RULE = "horizontal_rule";

const IMAGE = "image";

const RenderArray = ({ tag: Tag, content }) => (
  <Tag>{content.map((item, index) => (<Content key={String(index)} item={item} />))}</Tag>
);

const Text = ({ item }) => {
  let { text } = item;

  if (item.marks) {
    item.marks.forEach((mark) => {
      switch (mark.type) {
        case "bold":
          text = (<b>{text}</b>);
          break;
        case "italic":
          text = (<i>{text}</i>);
          break;
        case "strike":
          text = (<s>{text}</s>);
          break;
        case "underline":
          text = (<u>{text}</u>);
          break;
        case "link": {
          const { href, target } = mark.attrs;

          text = (<a href={href} target={target}>{text}</a>);
          break;
        }
        default:
          // eslint-disable-next-line no-console
          console.log("mark", mark);
          break;
      }
    });
  }

  return text;
};

const Content = ({ item }) => {
  switch (item.type) {
    case TYPE_DOC:
      return (<RenderArray tag="div" content={item.content} />);
    case TYPE_PARAGRAPH:
      return (<RenderArray tag="p" content={item.content} />);
    case TYPE_BULLET_LIST:
      return (<RenderArray tag="ul" content={item.content} />);
    case TYPE_LIST_ITEM:
      return (<RenderArray tag="li" content={item.content} />);
    case TYPE_HEADING:
      return (<RenderArray tag={`h${item.attrs.level}`} content={item.content} />);
    case ORDERED_LIST:
      return (<RenderArray tag="ol" content={item.content} />);
    case LIST_ITEM:
      return (<RenderArray tag="li" content={item.content} />);
    case BLOCKQUOTE:
      return (<RenderArray tag="blockquote" content={item.content} />);
    case TYPE_HARD_BREAK:
      return (<br />);
    case HORIZONTAL_RULE:
      return (<hr />);
    case IMAGE: {
      const { alt, src, title } = item.attrs;

      return (<img alt={alt} src={src} title={title} />);
    }
    case TYPE_TEXT:
      return (<Text item={item} />);
    default:
      // eslint-disable-next-line no-console
      console.log(">>>", item);

      return null;
  }
};

export default Content;
