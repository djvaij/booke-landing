import Css from "./style.module.scss";

import { useRouter } from "next/router";
import React from "react";

import Banner from "lib/common/Banner";
import Layout from "lib/common/Layout";
import NotFound from "pages/NotFound";

import Content from "./lib/Content";

const HubPost = ({ story, section }) => {
  const router = useRouter();

  if (router.isFallback) {
    return (
      <Layout>
        <div className="container">
          <p>Loading...</p>
        </div>
      </Layout>
    );
  }

  if (!story) {
    return (<NotFound />);
  }

  return (
    <Layout
      title={story.content.meta.title || "Resource hub"}
      description={story.content.meta.description}
      seo={story.content.meta}>
      <div className={Css.page}>
        <div className={`container ${Css.container}`}>
          <h1 className={Css.title}>{story.content.title}</h1>
          <div className={Css.breadcrumbs}>
            <a href="/hub" >Hub</a>
            <a href={`/hub/${(section && section.name) || ""}`}>{section && section.value}</a>
          </div>
          {!!(story.content.image && story.content.image.filename) && (
            <div className={Css.image}>
              <img src={story.content.image.filename} alt={story.content.title} />
            </div>
          )}
          <Content item={story.content.long_text} />
        </div>
        <Banner />
      </div>
    </Layout>
  );
};

export default HubPost;
