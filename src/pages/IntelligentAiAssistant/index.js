import CallToAction from "lib/common/CallToAction";
import Css from "./styles.module.scss";
import Feature from "lib/common/Feature";
import FormBlock from "lib/common/FormBlock";
import Layout from "lib/common/Layout";
import MainCustomized from "lib/common/MainCustomized";
import React from "react";

const featuresBackgrounds = {
  first: (
    <svg width="559" height="389" viewBox="0 0 559 389" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M191.038 384.205C195.762 387.874 201.881 389.236 207.715 387.917L542.996 312.134C554.151 309.613 560.924 298.268 557.851 287.252L484.477 24.2543C482.114 15.7847 474.496 9.8519 465.706 9.63504L89.9193 0.36438C81.1405 0.147797 73.2472 5.68314 70.4601 14.0107L1.50014 220.054C-1.19559 228.109 1.49007 236.987 8.19813 242.197L191.038 384.205Z"
        fill="#EFF5FF" />
    </svg>
  ),
  second: (
    <svg width="602" height="383" viewBox="0 0 602 383" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M71.2216 372.723C75.3016 379.57 83.0569 383.34 90.9613 382.32L584.346 318.665C595.615 317.211 603.411 306.672 601.503 295.471L566.081 87.5217C564.693 79.3733 558.435 72.9183 550.334 71.2779L200.607 0.465393C193.151 -1.04443 185.483 1.80713 180.825 7.82239L4.34459 235.724C-0.689589 242.225 -1.23206 251.145 2.97717 258.208L71.2216 372.723Z"
        fill="#EFF5FF" />
    </svg>
  )
};

const IntelligentAiAssistant = () => {
  return (
    <Layout title="Intelligent AI-assistant" index>
      <MainCustomized
        title={"Intelligent\n AI-assistant"}
        description={"Who checks the history of your reconciliation and\n suggests you the best options."}
        video={{
          webmUrl: "/video/intelligent-ai-assistant-main.webm",
          mpegUrl: "/video/intelligent-ai-assistant-main.mp4"
        }} />
      <div className="container">
        <Feature
          className={Css.feature}
          title="Red Warning"
          description={`Warned you about possible mistake and shows that
            the transaction with this Payee have never been
            categorized under selected account.`}
          video={{
            webmUrl: "/video/intelligent-ai-assistant-red-warning.webm",
            mpegUrl: "/video/intelligent-ai-assistant-red-warning.mp4"
          }}
          bgImage={featuresBackgrounds.first}
          bgPosition={{
            top: "-33px",
            left: "-33px"
          }} />
        <Feature
          reverse
          className={Css.feature}
          title="Yellow Warning"
          description={"Does the research and notifies you that the selected\n category is one of the many possible options."}
          video={{
            webmUrl: "/video/intelligent-ai-assistant-yellow-warning.webm",
            mpegUrl: "/video/intelligent-ai-assistant-yellow-warning.mp4"
          }}
          bgImage={featuresBackgrounds.second}
          bgPosition={{
            top: "-41px",
            left: "-40px"
          }} />
      </div>
      <CallToAction />
      <FormBlock />
    </Layout>
  );
};

export default IntelligentAiAssistant;
