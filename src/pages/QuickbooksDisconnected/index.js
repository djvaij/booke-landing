import Css from "./style.module.scss";

import React from "react";

import { AUTH_URL } from "utils/const";
import Layout from "lib/common/Layout";

const QuickbooksDisconnected = () => {
  return (
    <Layout title="QuickBooks Disconnected">
      <div className={Css.quickbooksDisconnected}>
        <div className={Css.container}>
          <h1>QuickBooks disconnected</h1>

          <h6>Your QuickBooks integration has been disconnected.
            You will no longer to be able to sync QuickBooks data with Booke.</h6>

          <p>If you’d like to reconnect Booke and your QuickBooks account, <a href={AUTH_URL}>sign in</a> and
          press “Add new business” button</p>
        </div>
      </div>
    </Layout>
  );
};

export default QuickbooksDisconnected;
