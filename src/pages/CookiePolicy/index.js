import React from "react";

import Layout from "lib/common/Layout";

import Content from "./Content";

const CookiePolicy = () => {
  return (
    <Layout title="Cookie policy">
      <div className="terms">
        <div className="container">
          <Content />
        </div>
      </div>
    </Layout>
  );
};

export default CookiePolicy;
