import CallToAction from "lib/common/CallToAction";
import Css from "./styles.module.scss";
import Feature from "lib/common/Feature";
import FormBlock from "lib/common/FormBlock";
import Layout from "lib/common/Layout";
import MainCustomized from "lib/common/MainCustomized";
import React from "react";
import classNames from "classnames";

const featuresBackgrounds = {
  first: (
    <svg width="561" height="403" viewBox="0 0 561 403" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M545.98 288.352L154.25 402.025C147.843 403.884 140.93 402.426 135.82 398.137L7.53491 290.478C1.95893 285.798 -0.634255 278.459 0.76441 271.315L39.6932 72.4845C41.3411 64.0679 48.1717 57.6389 56.6725 56.5033L471.293 1.11846C481.257 -0.21248 490.66 6.05886 493.26 15.7689L559.725 263.971C562.541 274.485 556.432 285.319 545.98 288.352Z"
        fill="#EFF5FF" />
    </svg>
  ),
  second: (
    <svg width="602" height="383" viewBox="0 0 602 383" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M71.2216 372.723C75.3016 379.57 83.0569 383.34 90.9613 382.32L584.346 318.665C595.615 317.211 603.411 306.672 601.503 295.471L566.081 87.5217C564.693 79.3733 558.435 72.9183 550.334 71.2779L200.607 0.465393C193.151 -1.04443 185.483 1.80713 180.825 7.82239L4.34459 235.724C-0.689589 242.225 -1.23206 251.145 2.97717 258.208L71.2216 372.723Z"
        fill="#EFF5FF" />
    </svg>
  ),
  third: (
    <svg width="560" height="389" viewBox="0 0 560 389" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M368.462 384.205C363.738 387.874 357.619 389.236 351.785 387.917L16.504 312.134C5.34888 309.613 -1.42432 298.268 1.64905 287.252L75.0232 24.2543C77.3861 15.7847 85.0038 9.8519 93.7942 9.63504L469.581 0.36438C478.36 0.147797 486.253 5.68314 489.04 14.0107L558 220.054C560.696 228.109 558.01 236.987 551.302 242.197L368.462 384.205Z"
        fill="#EFF5FF" />
    </svg>
  )
};

const PerformanceDashboard = () => {
  return (
    <Layout title="Performance Dashboard" index>
      <MainCustomized
        title={"Performance\n Dashboard"}
        description={"Manage and check the progress of all your\n clients in one place."}
        video={{
          webmUrl: "/video/performance-dashboard-hero.webm",
          mpegUrl: "/video/performance-dashboard-hero.mp4"
        }}
        leftClass={classNames(Css.left, Css.left)} />
      <div className="container">
        <Feature
          className={Css.feature}
          title="All clients in one place"
          description="Manage all your clients in one system, instead of multiple."
          video={{
            webmUrl: "/video/performance-dashboard-all-clients-in-one-place.webm",
            mpegUrl: "/video/performance-dashboard-all-clients-in-one-place.mp4"
          }}
          bgImage={featuresBackgrounds.first}
          bgPosition={{
            top: "-33px",
            left: "-24px"
          }} />
        <Feature
          reverse
          className={Css.feature}
          title={"Essential identifiers\n on one page"}
          description="Tracking performance of your clients is on one dashboard"
          video={{
            webmUrl: "/video/performance-dashboard-essential-identifiers.webm",
            mpegUrl: "/video/performance-dashboard-essential-identifiers.mp4"
          }}
          bgImage={featuresBackgrounds.second}
          bgPosition={{
            top: "-41px",
            left: "-40px"
          }} />
        <Feature
          className={Css.feature}
          title={"No need to go back\n and forth the pages"}
          description={"We saved you tons of time switching between\n different systems and businesses."}
          video={{
            webmUrl: "/video/performance-dashboard-no-need-to-go-back.webm",
            mpegUrl: "/video/performance-dashboard-no-need-to-go-back.mp4"
          }}
          bgImage={featuresBackgrounds.third}
          bgPosition={{
            top: "-36px",
            left: "-31px"
          }} />

      </div>
      <CallToAction />
      <FormBlock />
    </Layout>
  );
};

export default PerformanceDashboard;
