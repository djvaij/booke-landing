import CallToAction from "lib/common/CallToAction";
import Css from "./styles.module.scss";
import CustomAnimationBlock from "./lib/CustomAnimationBlock";
import Feature from "lib/common/Feature";
import FormBlock from "lib/common/FormBlock";
import Layout from "lib/common/Layout";
import MainCustomized from "lib/common/MainCustomized";
import React from "react";
import classNames from "classnames";

const featuresBackgrounds = {
  first: (
    <svg width="560" height="389" viewBox="0 0 560 389" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M368.462 384.205C363.738 387.874 357.619 389.236 351.785 387.917L16.504 312.134C5.34888 309.613 -1.42432 298.268 1.64905 287.252L75.0232 24.2543C77.3861 15.7847 85.0038 9.8519 93.7942 9.63504L469.581 0.36438C478.36 0.147797 486.253 5.68314 489.04 14.0107L558 220.054C560.696 228.109 558.01 236.987 551.302 242.197L368.462 384.205Z"
        fill="#EFF5FF" />
    </svg>
  ),
  second: (
    <svg width="602" height="383" viewBox="0 0 602 383" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M71.2216 372.723C75.3016 379.57 83.0569 383.34 90.9613 382.32L584.346 318.665C595.615 317.211 603.411 306.672 601.503 295.471L566.081 87.5217C564.693 79.3733 558.435 72.9183 550.334 71.2779L200.607 0.465393C193.151 -1.04443 185.483 1.80713 180.825 7.82239L4.34459 235.724C-0.689589 242.225 -1.23206 251.145 2.97717 258.208L71.2216 372.723Z"
        fill="#EFF5FF" />
    </svg>
  ),
  third: (
    <svg width="561" height="403" viewBox="0 0 561 403" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M545.984 114.888L154.254 1.21557C147.847 -0.643748 140.934 0.814378 135.824 5.10325L7.53882 112.763C1.96283 117.442 -0.630348 124.782 0.768316 131.926L39.6971 330.756C41.345 339.173 48.1756 345.602 56.6764 346.737L471.297 402.122C481.26 403.453 490.664 397.182 493.264 387.472L559.729 139.269C562.545 128.756 556.436 117.921 545.984 114.888Z"
        fill="#EFF5FF" />
    </svg>
  )
};

const TwoWayIntegrations = () => {
  return (
    <Layout title="Two-way integrations" index>
      <MainCustomized
        title={"Two-way\n integrations"}
        description="Seamlessly connect to Xero, QBO or QBD."
        customAnimationBlock={<CustomAnimationBlock />}
        rightClass={classNames(Css.right, Css.right)}
        leftClass={classNames(Css.left, Css.left)} />
      <div className="container">
        <Feature
          className={Css.feature}
          title={"Connect to your\n accounting system"}
          description={"Connection to your accounting system has never been\n that fast and easy!"}
          video={{
            webmUrl: "/video/two-way-integrations-connect-to-your-accounting-system.webm",
            mpegUrl: "/video/two-way-integrations-connect-to-your-accounting-system.mp4"
          }}
          bgImage={featuresBackgrounds.first}
          bgPosition={{
            top: "-33px",
            left: "-33px"
          }} />
        <Feature
          reverse
          className={Css.feature}
          title="Sync transactions"
          description={"Booke AI syncs all transactions automatically fast\n and hasstle-free."}
          video={{
            webmUrl: "/video/two-way-integrations-sync-transactions.webm",
            mpegUrl: "/video/two-way-integrations-sync-transactions.mp4"
          }}
          bgImage={featuresBackgrounds.second}
          bgPosition={{
            top: "-41px",
            left: "-40px"
          }} />
        <Feature
          className={Css.feature}
          title={"Sync Additional\n Information"}
          description={`Do not worry about the information of client’s Customers
            and Vendors. Booke AI has already synced it, you don’t even have to do anything.`}
          video={{
            webmUrl: "/video/two-way-integrations-sync-additional-information.webm",
            mpegUrl: "/video/two-way-integrations-sync-additional-information.mp4"
          }}
          bgImage={featuresBackgrounds.third}
          bgPosition={{
            top: "-33px",
            left: "-24px"
          }} />
      </div>
      <CallToAction />
      <FormBlock />
    </Layout>
  );
};

export default TwoWayIntegrations;
