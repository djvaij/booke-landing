import AnimationBlock from "lib/common/MainCustomized/lib/AnimationBlock";
import Css from "./styles.module.scss";
import React from "react";
import classNames from "classnames";

const CustomAnimationBlock = ({ className }) => {
  return (
    <div className={classNames(Css.container, className)}>
      <img className={Css.image} src="/images/two-way-integrations-hero.png" alt="Two-way integrations" />
      <div>
        <AnimationBlock
          videoUrl="/video/two-way-integrations-hero-animation.webm"
          movUrl="/video/two-way-integrations-hero-animation.mov"
          pngUrl="/images/two-way-integrations-hero-animation.png"
          className={Css.video}
          width="563"
          height="391"
          alt="Wwo Way Integrations" />
      </div>
    </div>
  );
};

export default CustomAnimationBlock;
