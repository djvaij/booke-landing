import Css from "./style.module.scss";

import { useRouter } from "next/router";
import React, { useCallback, useEffect, useRef, useState } from "react";

import { POSTS_PER_PAGE } from "const";
import Layout from "lib/common/Layout";
import Preloader from "lib/common/Preloader";

const Hub = ({ stories, sections }) => {
  const router = useRouter();

  const nodeRef = useRef();

  const [fetched, setFetched] = useState(true);

  const [hasMorePosts, setHasMorePosts] = useState(true);

  const [posts, setPosts] = useState(stories || []);

  const fetchPosts = useCallback(async() => {
    setFetched(true);
    try {
      const section = router.query.section ? `/${router.query.section}` : "";

      const resp = await fetch(`/api/hub${section}/page/${Math.ceil(posts.length / POSTS_PER_PAGE) + 1}`);

      const json = await resp.json();

      if (json.stories.length) {
        setHasMorePosts(!(posts.length % POSTS_PER_PAGE));
      } else {
        setHasMorePosts(false);
      }
      setPosts((prev) => [...prev, ...json.stories]);
    } catch (exeption) { }
  }, [posts.length, router.query.section]);

  const handleWindowScroll = useCallback(() => {
    if (!hasMorePosts || fetched || !nodeRef.current) return;

    if (nodeRef.current.getBoundingClientRect().top < window.innerHeight) {
      fetchPosts();
    }
  }, [hasMorePosts, fetched, fetchPosts]);

  useEffect(() => {
    window.addEventListener("scroll", handleWindowScroll);

    return () => {
      window.removeEventListener("scroll", handleWindowScroll);
    };
  }, [handleWindowScroll]);

  useEffect(() => {
    handleWindowScroll();
  }, [handleWindowScroll]);

  useEffect(() => {
    setFetched(false);
  }, [posts]);

  return (
    <Layout title="Resource hub">
      <div className={Css.page}>
        <div className="container">
          <div className={Css.row}>
            <div className={Css.aside}>
              <nav>
                <a href="/hub" className={`${Css.link} ${router.query.section ? "" : Css.active}`}>All articles</a>

                {sections && sections.filter(Boolean).map((section) => (
                  <a
                    key={section.name}
                    href={`/hub/${section.name}`}
                    className={`${Css.link} ${(router.query.section === section.name) ? Css.active : ""}`}>
                    {section.value}
                  </a>
                ))}
              </nav>
            </div>
            <div className={Css.main}>
              <div className={Css.posts}>
                {posts.filter(Boolean).map((story, index) => {
                  const section = sections.find((item) => item.value === story.content.section);

                  if (!section) return null;

                  const key = `${story.slug}-${index}`;

                  return (
                    <a
                      key={key}
                      href={`/hub/${section.name}/${story.slug}`}
                      className={Css.story}>
                      <div className={Css.card}>
                        <div className={Css.head}>
                          <h2 className={Css.title}>{story.content.title}</h2>
                          <div className={Css.readMore}>read more</div>
                        </div>
                        <div className={Css.image} style={{ backgroundImage: `url(${story.content.image_preview.filename})` }}>
                          {!!(story.content.image_preview && story.content.image_preview.filename) && (
                            <img src={story.content.image_preview.filename} alt={story.content.title} />
                          )}
                        </div>
                      </div>
                    </a>
                  );
                })}
              </div>

              {fetched
                ? (<Preloader />)
                : (<div className={Css.lazyLoad} ref={nodeRef} />)}
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default Hub;
