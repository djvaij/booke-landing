import Css from "./styles.module.scss";
import React from "react";
import WebmAnimation from "lib/common/WebmAnimation";

const MultiLanguagesAnimation = () => {
  return (
    <div className={Css.container}>
      <img className={Css.background} src="/images/invoice-and-receipt-ocr-ai-multi-languages-bg.svg" />
      <WebmAnimation
        className={Css.animation}
        height="383"
        webmUrl="/video/invoice-and-receipt-ocr-ai-multi-languages-animation.webm"
        movUrl="/video/invoice-and-receipt-ocr-ai-multi-languages-animation.mov"
        pngUrl="/images/invoice-and-receipt-ocr-ai-multi-languages-animation.png" />
      <img className={Css.image} src="/images/invoice-and-receipt-ocr-ai-multi-languages-image.svg" />
    </div>
  );
};

export default MultiLanguagesAnimation;
