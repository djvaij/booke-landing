import Css from "./styles.module.scss";
import React from "react";
import WebmAnimation from "lib/common/WebmAnimation";

const HeroAnimation = () => {
  return (
    <div className={Css.container}>
      <img className={Css.image} src="/images/invoice-and-receipt-ocr-ai-hero-image.svg" />
      <WebmAnimation
        className={Css.animation}
        height="436"
        webmUrl="/video/invoice-and-receipt-ocr-ai-hero-animation.webm"
        movUrl="/video/invoice-and-receipt-ocr-ai-hero-animation.mov"
        pngUrl="/images/invoice-and-receipt-ocr-ai-hero-animation.png" />
    </div>
  );
};

export default HeroAnimation;
