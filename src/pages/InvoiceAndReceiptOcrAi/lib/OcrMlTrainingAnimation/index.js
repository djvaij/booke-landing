import Css from "./styles.module.scss";
import React from "react";
import WebmAnimation from "lib/common/WebmAnimation";

const OcrMlTrainingAnimation = () => {
  return (
    <div className={Css.container}>
      <WebmAnimation
        className={Css.animation}
        webmUrl="/video/invoice-and-receipt-ocr-ai-ocr-ml-training-animation.webm"
        movUrl="/video/invoice-and-receipt-ocr-ai-ocr-ml-training-animation.mov"
        pngUrl="/images/invoice-and-receipt-ocr-ai-ocr-ml-training-animation.png" />
    </div>
  );
};

export default OcrMlTrainingAnimation;
