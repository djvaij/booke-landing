/* eslint-disable max-len */
import Css from "./styles.module.scss";
import React from "react";
import WebmAnimation from "lib/common/WebmAnimation";

const TaxRateAnimation = () => {
  return (
    <div className={Css.container}>
      <img className={Css.background} src="/images/invoice-and-receipt-ocr-ai-tax-rate-bg.svg" width="602" height="383" />
      <WebmAnimation
        className={Css.animation}
        height="382"
        webmUrl="/video/invoice-and-receipt-ocr-ai-tax-rate-animation.webm"
        movUrl="/video/invoice-and-receipt-ocr-ai-tax-rate-animation.mov"
        pngUrl="/images/invoice-and-receipt-ocr-ai-tax-rate-animation.png" />
      <img className={Css.image} src="/images/invoice-and-receipt-ocr-ai-tax-rate-image.svg" />
    </div>
  );
};

export default TaxRateAnimation;
