/* eslint-disable max-len */
import CallToAction from "lib/common/CallToAction";
import Css from "./styles.module.scss";
import Feature from "lib/common/Feature";
import FormBlock from "lib/common/FormBlock";
import HeroAnimation from "./lib/HeroAnimation";
import Layout from "lib/common/Layout";
import MainCustomized from "lib/common/MainCustomized";
import MultiLanguagesAnimation from "./lib/MultiLanguagesAnimation";
import OcrMlTrainingAnimation from "./lib/OcrMlTrainingAnimation";
import React from "react";
import TaxRateAnimation from "./lib/TaxRateAnimation";
import classNames from "classnames";

const featuresBackgrounds = {
  third: (
    <svg width="564" height="396" viewBox="0 0 564 396" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M497.059 11.2867C493.082 3.90528 484.924 -0.212692 476.624 0.972045L18.0181 66.4328C6.99817 68.0058 -0.616821 78.2736 1.07709 89.2756L34.4379 305.951C35.6906 314.087 41.8004 320.624 49.8331 322.424L374.134 395.071C382.044 396.843 390.242 393.665 394.891 387.023L559.931 151.231C564.265 145.038 564.738 136.932 561.154 130.277L497.059 11.2867Z" fill="#EFF5FF" />
    </svg>
  )
};

const InvoiceAndReceiptOcrAi = () => {
  return (
    <Layout title="Invoice & Receipt OCR AI" index>
      <MainCustomized
        title={"Invoice &\n Receipt OCR AI"}
        description="Real-time document data extraction."
        customAnimationBlock={<HeroAnimation />}
        rightClass={classNames(Css.right, Css.right)} />
      <div className="container">
        <Feature
          className={Css.feature}
          customAnimationBlock={<OcrMlTrainingAnimation />}
          title={"OCR ML training with\n custom document\n templates"}
          description={`Imagine a system that you can improve without even knowing it
            and doing your daily work. Booke AI learns from any Document
            you upload, and as a result, gives you better results.`} />
        <Feature
          reverse
          className={Css.feature}
          customAnimationBlock={<TaxRateAnimation />}
          title="Tax Rate"
          description={"Auto-completed based\n on QBO and Xero business settings"} />
        <Feature
          className={Css.feature}
          video={{
            webmUrl: "/video/invoice-and-receipt-ocr-ai-confidence-level.webm",
            mpegUrl: "/video/invoice-and-receipt-ocr-ai-confidence-level.mp4"
          }}
          bgImage={featuresBackgrounds.third}
          bgPosition={{
            top: "-28px",
            left: "-36px"
          }}
          title={"Confidence\n level for each field"}
          description={"We know and show how certain we‘re about each field\n fulfilled automatically."} />
        <Feature
          reverse
          className={Css.feature}
          customAnimationBlock={<MultiLanguagesAnimation />}
          title={"Multi-languages and\n currencies support"}
          description={`Your clients get Documents in multiple languages? Yo
            provide your services to companies on different
            continents? We’ve got you covered!`} />
        
      </div>
      <CallToAction />
      <FormBlock />
    </Layout>
  );
};

export default InvoiceAndReceiptOcrAi;
