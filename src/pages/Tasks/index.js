import CallToAction from "lib/common/CallToAction";
import Css from "./styles.module.scss";
import Feature from "lib/common/Feature";
import FormBlock from "lib/common/FormBlock";
import Layout from "lib/common/Layout";
import MainCustomized from "lib/common/MainCustomized";
import React from "react";
import classNames from "classnames";

const featuresBackgrounds = {
  first: (
    <svg width="559" height="389" viewBox="0 0 559 389" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M191.038 384.205C195.762 387.874 201.881 389.236 207.716 387.917L542.996 312.134C554.151 309.613 560.924 298.268 557.851 287.252L484.477 24.2543C482.114 15.7847 474.496 9.85189 465.706 9.63503L89.9193 0.364377C81.1405 0.147803 73.2472 5.68313 70.4601 14.0106L1.50014 220.054C-1.19559 228.109 1.49007 236.987 8.19813 242.197L191.038 384.205Z"
        fill="#EFF5FF" />
    </svg>
  ),
  second: (
    <svg width="602" height="383" viewBox="0 0 602 383" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M71.2216 372.723C75.3016 379.57 83.0569 383.34 90.9613 382.32L584.346 318.665C595.615 317.211 603.411 306.672 601.503 295.471L566.081 87.5217C564.693 79.3733 558.435 72.9183 550.334 71.2779L200.607 0.465393C193.151 -1.04443 185.483 1.80713 180.825 7.82239L4.34459 235.724C-0.689589 242.225 -1.23206 251.145 2.97717 258.208L71.2216 372.723Z"
        fill="#EFF5FF" />
    </svg>
  )
};

const Tasks = () => {
  return (
    <Layout title="Tasks" index>
      <MainCustomized
        title="Tasks"
        description="Reduce internal and external communication."
        video={{
          webmUrl: "/video/tasks-hero.webm",
          mpegUrl: "/video/tasks-hero.mp4"
        }}
        leftClass={classNames(Css.left, Css.left)} />
      <div className="container">
        <Feature
          className={Css.feature}
          video={{
            webmUrl: "/video/tasks-internal-communication.webm",
            mpegUrl: "/video/tasks-internal-communication.mp4"
          }}
          bgImage={featuresBackgrounds.first}
          bgPosition={{
            top: "-36px",
            left: "-33px"
          }}
          title="Internal communication"
          description={`Booke AI encourages smooth and effective collaboration within
            the team to promote health relationships.`} />
        <Feature
          reverse
          className={Css.feature}
          video={{
            webmUrl: "/video/tasks-external-communication.webm",
            mpegUrl: "/video/tasks-external-communication.mp4"
          }}
          bgImage={featuresBackgrounds.second}
          bgPosition={{
            top: "-41px",
            left: "-40px"
          }}
          title={"External\n communication"}
          description={"connect one-on-one with your clients regarding\n any request."} />

      </div>
      <CallToAction />
      <FormBlock />
    </Layout>
  );
};

export default Tasks;
