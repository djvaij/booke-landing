import Css from "./style.module.scss";

import React from "react";

import CallToAction from "lib/common/CallToAction";
import ContactForm from "lib/common/ContactForm";
import Layout from "lib/common/Layout";
import Pricing from "lib/common/Pricing";
import Testimonials from "lib/common/Testimonials";

const Demo = () => {
  return (
    <Layout title="Product Demo">
      <div className={Css.demo}>
        <div className="container">
          <div className={Css.videoBlock}>
            <h1>Fixing uncategorized transactions with <span>Booke.ai</span> in action</h1>
            <p>Forget about phone calls and spreadsheets, ask your clients in one click</p>
            <div className={Css.row}>
              <div className={Css.colVideo}>
                <iframe
                  src="https://player.vimeo.com/video/670419437?h=f291f4da97&title=0&byline=0&portrait=0"
                  frameBorder="0"
                  allowFullScreen />
              </div>
              <div className={Css.colForm}>
                <div className={Css.form}>
                  <h2>Request a Trial</h2>
                  <h3>Make the first step to no stress accounting with Booke</h3>
                  <ContactForm withCompanyName />
                </div>
              </div>
            </div>
          </div>
        </div>

        <Testimonials />

        <Pricing />

        <CallToAction />
      </div>
    </Layout>
  );
};

export default Demo;
