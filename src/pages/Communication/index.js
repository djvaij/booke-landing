import CallToAction from "lib/common/CallToAction";
import Css from "./styles.module.scss";
import Feature from "lib/common/Feature";
import FormBlock from "lib/common/FormBlock";
import Layout from "lib/common/Layout";
import MainCustomized from "lib/common/MainCustomized";
import React from "react";
import classNames from "classnames";

const featuresBackgrounds = {
  first: (
    <svg width="602" height="383" viewBox="0 0 602 383" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M530.778 10.2767C526.698 3.43041 518.943 -0.34006 511.039 0.679741L17.6538 64.3353C6.38458 65.7892 -1.41107 76.328 0.496948 87.5293L35.9191 295.478C37.3071 303.627 43.5648 310.082 51.6661 311.722L401.393 382.535C408.849 384.044 416.517 381.193 421.175 375.178L597.655 147.276C602.69 140.775 603.232 131.855 599.023 124.792L530.778 10.2767Z"
        fill="#EFF5FF" />
    </svg>
  ),
  second: (
    <svg width="561" height="403" viewBox="0 0 561 403" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M14.6452 288.352L406.375 402.025C412.782 403.884 419.695 402.426 424.805 398.137L553.09 290.478C558.666 285.798 561.259 278.459 559.861 271.315L520.932 72.4842C519.284 64.0677 512.453 57.6386 503.953 56.5031L89.3321 1.11821C79.3684 -0.212724 69.9649 6.05862 67.3647 15.7687L0.899659 263.971C-1.91569 274.484 4.19269 285.319 14.6452 288.352Z"
        fill="#EFF5FF" />
    </svg>
  ),
  third: (
    <svg width="561" height="403" viewBox="0 0 561 403" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M14.6452 288.352L406.375 402.025C412.782 403.884 419.695 402.426 424.805 398.137L553.09 290.478C558.666 285.798 561.259 278.459 559.861 271.315L520.932 72.4842C519.284 64.0677 512.453 57.6386 503.953 56.5031L89.3321 1.11821C79.3684 -0.212724 69.9649 6.05862 67.3647 15.7687L0.899659 263.971C-1.91569 274.484 4.19269 285.319 14.6452 288.352Z"
        fill="#EFF5FF" />
    </svg>
  ),
  fourth: (
    <svg width="559" height="389" viewBox="0 0 559 389" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M191.038 4.79537C195.762 1.12619 201.881 -0.235809 207.715 1.08296L542.996 76.8655C554.151 79.3869 560.924 90.7321 557.851 101.748L484.477 364.746C482.114 373.215 474.496 379.148 465.706 379.365L89.9193 388.636C81.1405 388.852 73.2472 383.317 70.4601 374.989L1.50014 168.946C-1.19559 160.891 1.49007 152.013 8.19813 146.803L191.038 4.79537Z"
        fill="#EFF5FF" />
    </svg>
  )
};

const Communication = () => {
  return (
    <Layout title="Communication" index>
      <MainCustomized
        title="Communication"
        description="Ask your client about transactions in one click."
        video={{
          webmUrl: "/video/communication-hero.webm",
          mpegUrl: "/video/communication-hero.mp4"
        }}
        leftClass={classNames(Css.left, Css.left)} />
      <div className="container">
        <Feature
          title={"Communicate with\n your clients faster"}
          description={`One click and better P&L, less manual work, thankful
            clients VS many emails, phone call, human errors in the
            categorization, stressed out clients. The choice is yours!`}
          video={{
            webmUrl: "/video/communication-communicate-with-your-clients-faster.webm",
            mpegUrl: "/video/communication-communicate-with-your-clients-faster.mp4"
          }}
          bgImage={featuresBackgrounds.first}
          bgPosition={{
            top: "-40px",
            left: "-41px"
          }} />
        <Feature
          reverse
          title={"Who do you\n want to ask?"}
          description={"Select who exactly you want to send\n your transactions to."}
          video={{
            webmUrl: "/video/communication-who-do-you-want-to-ask.webm",
            mpegUrl: "/video/communication-who-do-you-want-to-ask.mp4"
          }}
          bgImage={featuresBackgrounds.second}
          bgPosition={{
            top: "-28px",
            left: "-46px"
          }} />
        <Feature
          title={"Keep track of all\n your transactions sent"}
          description={`Booke AI shows all transactions sent to clients, when and whom
            they have been sent. And, on top of it, when the last email
            reminder was generated.`}
          video={{
            webmUrl: "/video/communication-keep-track-of-all-your-transactions-sent.webm",
            mpegUrl: "/video/communication-keep-track-of-all-your-transactions-sent.mp4"
          }}
          bgImage={featuresBackgrounds.third}
          bgPosition={{
            top: "-28px",
            left: "-46px"
          }} />
        <Feature
          reverse
          title={"Take control over\n what your clients can\n see and how they can react"}
          description={"Customize your clients’ view: add or remove any field\n with one click."}
          video={{
            webmUrl: "/video/communication-take-control-over.webm",
            mpegUrl: "/video/communication-take-control-over.mp4"
          }}
          bgImage={featuresBackgrounds.fourth}
          bgPosition={{
            top: "-33px",
            left: "-33px"
          }} />
        
      </div>
      <CallToAction />
      <FormBlock />
    </Layout>
  );
};

export default Communication;
