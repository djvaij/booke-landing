import Css from "./style.module.scss";

import React, { useCallback, useEffect, useRef, useState } from "react";
import classNames from "classnames";

import { preloadImages } from "utils";
import InViewport from "lib/common/InViewport";

import ITEMS from "./slides.json";

const SLIDE_CHANGE_TIME = 5000;

const ANIMATION_TIME = 200;

const Item = ({ index, active, onClick }) => {
  const handleItemClick = useCallback(() => {
    onClick(index);
  }, [index, onClick]);

  return (
    <div
      className={classNames(Css.item, active && Css.active)}
      onClick={handleItemClick}>
      <span />
    </div>
  );
};

const Slider = () => {
  const [slideIndex, setCurrentSlide] = useState(0);

  const [animated, setAnimated] = useState(false);

  const [hovered, setHovered] = useState();

  const [imageLoaded, setImageLoaded] = useState(true);

  const animationTimeoutIdRef = useRef();

  const animationTimeoutIdCbRef = useRef();

  const autoAnimationTimeoutIdRef = useRef();

  const runAnimation = useCallback((callback) => {
    if (animationTimeoutIdCbRef.current) {
      animationTimeoutIdCbRef.current();
    }
    clearTimeout(animationTimeoutIdRef.current);
    setAnimated(true);

    const timeoutCallback = () => {
      callback();
      setAnimated(false);
      animationTimeoutIdCbRef.current = null;
    };

    animationTimeoutIdCbRef.current = timeoutCallback;
    animationTimeoutIdRef.current = setTimeout(timeoutCallback, ANIMATION_TIME);
  }, []);

  const handleItemClick = useCallback((index) => {
    runAnimation(() => {
      setCurrentSlide(index);
    });
  }, [runAnimation]);

  const handlePrevClick = useCallback(() => {
    runAnimation(() => {
      setImageLoaded(false);
      setCurrentSlide((prev) => {
        const next = prev - 1;

        return next < 0 ? ITEMS.length - 1 : next;
      });
    });
  }, [runAnimation]);

  const handleNextClick = useCallback(() => {
    runAnimation(() => {
      setImageLoaded(false);
      setCurrentSlide((prev) => {
        const next = prev + 1;

        return next === ITEMS.length ? 0 : next;
      });
    });
  }, [runAnimation]);

  const handleMouseEnter = useCallback(() => {
    setHovered(true);
  }, []);

  const handleMouseLeave = useCallback(() => {
    setHovered(false);
  }, []);

  const handleImageLoad = useCallback(() => {
    setImageLoaded(true);
  }, []);

  useEffect(() => {
    clearTimeout(autoAnimationTimeoutIdRef.current);
    if (hovered) return;
    autoAnimationTimeoutIdRef.current = setTimeout(() => {
      handleNextClick();
    }, SLIDE_CHANGE_TIME);
  }, [slideIndex, hovered, handleNextClick]);

  useEffect(() => {
    preloadImages(ITEMS.map((item) => item.imgSrc));
  }, []);

  const { imgSrc, iconSrc, title, description } = ITEMS[slideIndex];

  return (
    <div className={Css.slider}>
      <InViewport>
        {({ shownOnce }) => (
          <div className="container">
            <div className={`${Css.container} fadeIn ${!shownOnce && "notShown"}`}>
              <div className={Css.left}>
                <div className={Css.textLarge}>5 tools for 5x productivity</div>
                <div className={classNames(Css.itemContent, animated && Css.animated)}>
                  <div className={Css.title}>
                    <div className={Css.icon}>
                      <img src={iconSrc} />
                    </div>
                    {title}
                  </div>
                  <div className={Css.description}>{description}</div>
                </div>
                <div className={Css.control}>
                  <div className={Css.prev} onClick={handlePrevClick}>
                    <img src="/images/icon-caret-left.svg" />
                  </div>
                  {ITEMS.map((item, index) => (
                    <Item
                      key={item.imgSrc}
                      index={index}
                      active={slideIndex === index}
                      onClick={handleItemClick} />
                  ))}
                  <div className={Css.next} onClick={handleNextClick}>
                    <img
                      src="/images/icon-caret-right.svg"
                      onMouseEnter={handleMouseEnter}
                      onMouseLeave={handleMouseLeave} />
                  </div>
                </div>
              </div>
              <div className={Css.right}>
                <div className={classNames(Css.imageWrap, animated && Css.animated)}>
                  <img
                    src={imgSrc}
                    className={classNames(imageLoaded && Css.imageLoaded)}
                    onLoad={handleImageLoad} />
                </div>
              </div>
              <div className={Css.mobileTitle}>5 tools for x5 productivity</div>
            </div>
          </div>
        )}
      </InViewport>
    </div>
  );
};

export default Slider;
