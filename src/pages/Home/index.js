import React from "react";

import CallToAction from "lib/common/CallToAction";
import Counters from "lib/common/Counters";
import Features from "lib/common/Features";
import FormBlock from "lib/common/FormBlock";
import Infographics from "lib/common/Infographics";
import Layout from "lib/common/Layout";
import Main from "lib/common/Main";
import Pricing from "lib/common/Pricing";
import VideoFeedback from "lib/common/VideoFeedback";

import Slider from "./lib/Slider";

const Home = () => {
  return (
    <Layout index title="For Accountants">
      <Main />
      <Infographics />
      <Features />
      <VideoFeedback />
      <Slider />
      <Counters />
      <Pricing />
      <CallToAction />
      <FormBlock />
    </Layout>
  );
};

export default Home;
