import Css from "./styles.module.scss";
import React from "react";
import useIos from "hooks/useIos";

const HeroAnimation = () => {
  const { iosPlatform } = useIos();

  if (iosPlatform) {
    return (
      <img
        className={Css.video}
        width="623"
        height="388"
        src="/video/changes-logging-hero.mov"
        alt="Changes Logging" />
    );
  }

  return (
    <div className={Css.container}>
      <video width="623" height="388" className={Css.video} autoPlay muted loop>
        <source src="/video/changes-logging-hero.webm" type="video/webm" />
        <source src="/video/changes-logging-hero.mp4" type="video/mp4" />
      </video>
    </div>
  );
};

export default HeroAnimation;
