import CallToAction from "lib/common/CallToAction";
import Css from "./styles.module.scss";
import Feature from "lib/common/Feature";
import FormBlock from "lib/common/FormBlock";
import HeroAnimation from "./lib/HeroAnimation";
import Layout from "lib/common/Layout";
import MainCustomized from "lib/common/MainCustomized";
import React from "react";
import classNames from "classnames";

const featuresBackgrounds = {
  first: (
    <svg width="560" height="389" viewBox="0 0 560 389" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M368.462 4.79537C363.738 1.12619 357.619 -0.235809 351.785 1.08296L16.504 76.8655C5.34888 79.3869 -1.42432 90.7321 1.64905 101.748L75.0232 364.746C77.3861 373.215 85.0038 379.148 93.7942 379.365L469.581 388.636C478.36 388.852 486.253 383.317 489.04 374.989L558 168.946C560.696 160.891 558.01 152.013 551.302 146.803L368.462 4.79537Z"
        fill="#EFF5FF" />
    </svg>
  )
};

const ChangesLogging = () => {
  return (
    <Layout title="Changes Logging" index>
      <MainCustomized
        title="Changes Logging"
        description="We log all the changes so that you don’t have to."
        customAnimationBlock={<HeroAnimation />}
        containerClass={Css.mainCustomizedContainer}
        rightClass={classNames(Css.right, Css.right, Css.right)}
        leftClass={classNames(Css.left, Css.left, Css.left)} />
      <div className="container">
        <Feature
          video={{
            webmUrl: "/video/changes-logging-document-changes.webm",
            mpegUrl: "/video/changes-logging-document-changes.mp4"
          }}
          bgImage={featuresBackgrounds.first}
          bgPosition={{
            top: "-33px",
            left: "-33px"
          }}
          title="Document changes"
          description={`We know who, when and how made changes to
            any document.`} />

      </div>
      <CallToAction />
      <FormBlock />
    </Layout>
  );
};

export default ChangesLogging;
