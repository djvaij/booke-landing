/* eslint-disable max-len */
import BulkUploadDocumentsAnimation from "./lib/BulkUploadDocumentsAnimation";
import CallToAction from "lib/common/CallToAction";
import Css from "./styles.module.scss";
import ExportDocumentAnimation from "./lib/ExportDocumentAnimation";
import Feature from "lib/common/Feature";
import FormBlock from "lib/common/FormBlock";
import GoogleDriveAndDropboxAnimation from "./lib/GoogleDriveAndDropboxAnimation";
import HeroAnimation from "./lib/HeroAnimation";
import Layout from "lib/common/Layout";
import MainCustomized from "lib/common/MainCustomized";
import React from "react";
import classNames from "classnames";

const featuresBackgrounds = {
  second: (
    <svg width="602" height="383" viewBox="0 0 602 383" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M530.778 10.2767C526.698 3.43041 518.943 -0.34006 511.039 0.679741L17.6538 64.3353C6.38458 65.7892 -1.41107 76.328 0.496948 87.5293L35.9191 295.478C37.3071 303.627 43.5648 310.082 51.6661 311.722L401.393 382.535C408.849 384.044 416.517 381.193 421.175 375.178L597.655 147.276C602.69 140.775 603.232 131.855 599.023 124.792L530.778 10.2767Z" fill="#EFF5FF" />
    </svg>
  ),
  third: (
    <svg width="561" height="403" viewBox="0 0 561 403" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M14.6452 114.888L406.375 1.21569C412.782 -0.643626 419.695 0.8145 424.805 5.10337L553.09 112.763C558.666 117.442 561.259 124.782 559.861 131.926L520.932 330.756C519.284 339.173 512.453 345.602 503.953 346.738L89.3321 402.122C79.3684 403.453 69.9649 397.182 67.3647 387.472L0.899659 139.27C-1.91569 128.756 4.19269 117.922 14.6452 114.888Z" fill="#EFF5FF" />
    </svg>
  )
};

const AccountsPayableWorkflow = () => {
  return (
    <Layout title="Accounts Payable workflow" index>
      <MainCustomized
        title={"Accounts Payable\n workflow"}
        description="Import and manage documents easy and in bulk."
        customAnimationBlock={<HeroAnimation />}
        rightClass={classNames(Css.right, Css.right)} />
      <div className="container">
        <Feature
          className={Css.feature}
          title="Bulk Upload Documents"
          description={`If you’re uploading documents one-by-one, then you’re
            at the right place. Upload them to Booke AI in bulk with
            a couple clicks.`}
          customAnimationBlock={<BulkUploadDocumentsAnimation />} />
        <Feature
          reverse
          className={Css.feature}
          title={"Unique email to share\n with your clients"}
          description={`Tired of downloading-uploading-transfering clients’
            documents? Our sophisticated platform generates a
            unique email address that you can share with your clients
            to upload the Docs and they will automatically go to
            Booke. Magic? No, 21st century.`}
          video={{
            webmUrl: "/video/accounts-payable-workflow-unique-email-to-share.webm",
            mpegUrl: "/video/accounts-payable-workflow-unique-email-to-share.mp4"
          }}
          bgImage={featuresBackgrounds.second}
          bgPosition={{
            top: "-41px",
            left: "-40px"
          }} />
        <Feature
          className={Css.feature}
          title={"Different workflows\n to choose from"}
          description={`We offer different Flows to work with Documents. Choose Basic for
            a simplified flow without editing and exporting. And Advanced, on
            the contrary. Make your choice!`}
          video={{
            webmUrl: "/video/accounts-payable-workflow-different-workflows.webm",
            mpegUrl: "/video/accounts-payable-workflow-different-workflows.mp4"
          }}
          bgImage={featuresBackgrounds.third}
          bgPosition={{
            top: "-34px",
            left: "-51px"
          }} />
        <Feature
          reverse
          className={Css.feature}
          title={"Export with\n Original Document"}
          description={"So that you always have a reference to a original\n document."}
          customAnimationBlock={<ExportDocumentAnimation />} />
        <Feature
          className={Css.feature}
          title={"Google Drive and\n DropBox Integration"}
          description={`We offer different Flows to work with Documents.
            Choose Basic for a simplified flow without editing and
            exporting. And Advanced, on the contrary. Make your choice!`}
          customAnimationBlock={<GoogleDriveAndDropboxAnimation />} />
      </div>
      <CallToAction />
      <FormBlock />
    </Layout>
  );
};

export default AccountsPayableWorkflow;
