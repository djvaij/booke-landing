import Css from "./styles.module.scss";
import React from "react";
import WebmAnimation from "lib/common/WebmAnimation";
import classNames from "classnames";

const HeroAnimation = ({ className }) => {
  return (
    <div className={classNames(Css.container, className)}>
      <WebmAnimation
        height="575"
        className={Css.video}
        webmUrl="/video/accounts-payable-workflow-hero.webm"
        movUrl="/video/accounts-payable-workflow-hero.mov"
        pngUrl="/images/accounts-payable-workflow-hero.png" />
      <img className={Css.image} src="/images/accounts-payable-workflow-hero-logo.png" alt="logo" />
    </div>
  );
};

export default HeroAnimation;
