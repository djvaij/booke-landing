import Css from "./styles.module.scss";
import React from "react";
import WebmAnimation from "lib/common/WebmAnimation";

const GoogleDriveAndDropboxAnimation = () => {
  return (
    <div className={Css.container}>
      <img className={Css.background} src="/images/accounts-payable-workflow-google-drive-and-dropbox-integration-bg.svg" />
      <WebmAnimation
        className={Css.animation}
        webmUrl="/video/accounts-payable-workflow-google-drive-and-dropbox-integration.webm"
        movUrl="/video/accounts-payable-workflow-google-drive-and-dropbox-integration.mov"
        pngUrl="/images/accounts-payable-workflow-google-drive-and-dropbox-integration.png" />
      <img className={Css.image} src="/images/accounts-payable-workflow-google-drive-and-dropbox-integration-image.svg" />
    </div>
  );
};

export default GoogleDriveAndDropboxAnimation;
