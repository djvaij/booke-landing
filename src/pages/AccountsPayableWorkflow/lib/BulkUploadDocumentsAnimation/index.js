/* eslint-disable max-len */
import Css from "./styles.module.scss";
import React from "react";
import WebmAnimation from "lib/common/WebmAnimation";

const BulkUploadDocumentsAnimation = () => {
  return (
    <div className={Css.container}>
      <WebmAnimation
        height="389"
        className={Css.animation}
        webmUrl="/video/accounts-payable-workflow-bulk-upload-documents.webm"
        movUrl="/video/accounts-payable-workflow-bulk-upload-documents.mov"
        pngUrl="/images/accounts-payable-workflow-bulk-upload-documents.png" />
    </div>
  );
};

export default BulkUploadDocumentsAnimation;
