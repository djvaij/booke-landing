/* eslint-disable max-len */
import Css from "./styles.module.scss";
import React from "react";
import WebmAnimation from "lib/common/WebmAnimation";

const ExportDocumentAnimation = () => {
  return (
    <div className={Css.container}>
      <img className={Css.background} src="/images/accounts-payable-workflow-export-with-original-document-bg.svg" />
      <WebmAnimation
        className={Css.animation}
        height="386"
        webmUrl="/video/accounts-payable-workflow-export-with-original-document.webm"
        movUrl="/video/accounts-payable-workflow-export-with-original-document.mov"
        pngUrl="/images/accounts-payable-workflow-export-with-original-document.png" />
      <img className={Css.image} src="/images/accounts-payable-workflow-export-with-original-document-image.svg" />
    </div>

  );
};

export default ExportDocumentAnimation;
