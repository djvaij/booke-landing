import CallToAction from "lib/common/CallToAction";
import Css from "./styles.module.scss";
import Feature from "lib/common/Feature";
import FormBlock from "lib/common/FormBlock";
import Layout from "lib/common/Layout";
import MainCustomized from "lib/common/MainCustomized";
import React from "react";

const featuresBackgrounds = {
  first: (
    <svg width="560" height="389" viewBox="0 0 560 389" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M368.462 4.79537C363.738 1.12619 357.619 -0.235809 351.785 1.08296L16.504 76.8655C5.34888 79.3869 -1.42432 90.7321 1.64905 101.748L75.0232 364.746C77.3861 373.215 85.0038 379.148 93.7942 379.365L469.581 388.636C478.36 388.852 486.253 383.317 489.04 374.989L558 168.946C560.696 160.891 558.01 152.013 551.302 146.803L368.462 4.79537Z"
        fill="#EFF5FF" />
    </svg>
  ),
  second: (
    <svg width="602" height="383" viewBox="0 0 602 383" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M530.778 10.2767C526.698 3.43041 518.943 -0.34006 511.039 0.679741L17.6538 64.3353C6.38457 65.7892 -1.41107 76.328 0.496974 87.5293L35.9191 295.478C37.3071 303.627 43.5647 310.082 51.6661 311.722L401.393 382.535C408.849 384.044 416.517 381.193 421.175 375.178L597.655 147.276C602.69 140.775 603.232 131.855 599.023 124.792L530.778 10.2767Z"
        fill="#EFF5FF" />
    </svg>
  ),
  third: (
    <svg width="561" height="403" viewBox="0 0 561 403" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M545.98 288.352L154.25 402.025C147.843 403.884 140.93 402.426 135.82 398.137L7.53491 290.478C1.95893 285.798 -0.634255 278.459 0.76441 271.315L39.6932 72.4843C41.3411 64.0678 48.1717 57.6388 56.6725 56.5032L471.293 1.11833C481.257 -0.212602 490.66 6.05874 493.26 15.7688L559.725 263.971C562.541 274.485 556.432 285.319 545.98 288.352Z"
        fill="#EFF5FF" />
    </svg>
  ),
  fourth: (
    <svg width="602" height="383" viewBox="0 0 602 383" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M530.778 372.723C526.698 379.57 518.943 383.34 511.039 382.32L17.6538 318.665C6.38458 317.211 -1.41107 306.672 0.496948 295.471L35.9191 87.5217C37.3071 79.3733 43.5648 72.9183 51.6661 71.2779L401.393 0.465393C408.849 -1.04443 416.517 1.80713 421.175 7.82239L597.655 235.724C602.69 242.225 603.232 251.145 599.023 258.208L530.778 372.723Z"
        fill="#EFF5FF" />
    </svg>
  )
};

const AutoCategorization = () => {
  return (
    <Layout title="Auto-categorization" index>
      <MainCustomized
        title={"Auto-\ncategorization"}
        description={`Categorize transactions x5 faster/With 100% certainty
          we know how you categorize and therefore can do the
          majority of the work for you!`}
        animationResources={{
          videoUrl: "/video/auto-categorization.webm",
          movUrl: "/video/auto-categorization.mov",
          pngUrl: "/images/auto-categorization.png"
        }}
        animationClass={Css.video} />
      <div className="container">
        <Feature
          className={Css.feature}
          title="Category"
          description={"Booke AI is using ML and AI to suggest you \n the best Category."}
          video={{
            webmUrl: "/video/auto-categorization-category.webm",
            mpegUrl: "/video/auto-categorization-category.mp4"
          }}
          bgImage={featuresBackgrounds.first}
          bgPosition={{
            top: "-33px",
            left: "-33px"
          }} />
        <Feature
          className={Css.feature}
          reverse
          title="Class, Project, Location"
          description={`Booke AI treats each field equally, so optional fields will
            have their suggestions based on the historical
            reconciliation as well.`}
          video={{
            webmUrl: "/video/auto-categorization-class-project-location.webm",
            mpegUrl: "/video/auto-categorization-class-project-location.mp4"
          }}
          bgImage={featuresBackgrounds.second}
          bgPosition={{
            top: "-41px",
            left: "-40px"
          }} />
        <Feature
          className={Css.feature}
          title="Tax Rate"
          description={`Doesn’t matter where your company is located: USA, Canada,
            Australia or New Zealand. Booke AI gets your Taxes covered.`}
          video={{
            webmUrl: "/video/auto-categorization-tax-rate.webm",
            mpegUrl: "/video/auto-categorization-tax-rate.mp4"
          }}
          bgImage={featuresBackgrounds.third}
          bgPosition={{
            top: "-30px",
            left: "-24px"
          }} />
        <Feature
          className={Css.feature}
          reverse
          title="Based on client’s comment"
          description={`You don’t even have to do anything. Your client leaves
            a comment to the transaction and Booke AI
            categorizes it automatically.`}
          video={{
            webmUrl: "/video/auto-categorization-based-on-clients-comment.webm",
            mpegUrl: "/video/auto-categorization-based-on-clients-comment.mp4"
          }}
          bgImage={featuresBackgrounds.fourth}
          bgPosition={{
            top: "-41px",
            left: "-40px"
          }} />
      </div>
      <CallToAction />
      <FormBlock />
    </Layout>
  );
};

export default AutoCategorization;
