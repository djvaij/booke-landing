import Css from "./style.module.scss";

import * as CompanyEmailValidator from "company-email-validator";
import React, { useCallback } from "react";

import { HTTPS_STATUS_TOO_MANY_REQUESTS } from "utils/request";
import { IconBriefCase, IconGift, IconMail, IconPhone, IconUser } from "lib/icons";
import { sendContactFormData } from "utils/api";
import { showDialog } from "lib/common/Dialogs";
import Form, { FormField } from "lib/common/Form";
import validate from "utils/validate";

const INITIAL_VALUES = {
  userEmail: "",
  userName: "",
  companyName: "",
  userPhone: "",
  promoCode: ""
};

const validator = (values) => {
  const errors = validate(values, {
    userEmail: { required: true, email: true },
    userName: { required: true },
    companyName: { required: true },
    userPhone: { required: true }
  });

  if (!CompanyEmailValidator.isCompanyEmail(values.userEmail)) {
    showDialog({
      message: `Please enter your company's email address.
  Emails from free services such as gmail, yahoo, and others aren’t accepatble`
    });

    errors.userEmail = true;
  }

  return errors;
};

const RequestForm = ({ onFormSend }) => {
  const handleSubmit = useCallback(async(values) => {
    try {
      const response = await sendContactFormData(values);

      if (!response.sent) throw response;
      window.gtag("event", "contact_form_submitted");
      onFormSend();
    } catch (exception) {
      // eslint-disable-next-line no-console
      console.log("exception", exception);
      if (exception.status === HTTPS_STATUS_TOO_MANY_REQUESTS) {
        showDialog({ message: "Too many requests. Try again later." });
      } else {
        showDialog({ message: "Something went wrong. Try again later." });
      }

      return {};
    }

    return null;
  }, [onFormSend]);

  return (
    <Form
      className={Css.requestForm}
      initialValues={INITIAL_VALUES}
      validator={validator}
      onSubmit={handleSubmit}>
      <FormField
        type="email"
        name="userEmail"
        placeholder="Enter your business email"
        icon={<IconMail />} />
      <FormField
        name="userName"
        placeholder="Enter your full name"
        icon={<IconUser />} />
      <FormField
        name="companyName"
        placeholder="Enter your company name"
        icon={<IconBriefCase />} />
      <FormField
        type="phone"
        name="userPhone"
        icon={<IconPhone />}
        placeholder="Enter your phone" />
      <FormField
        name="promoCode"
        placeholder="Promo code (optional)"
        icon={<IconGift />} />
      <button className="block" type="submit">Request</button>
    </Form>
  );
};

export default RequestForm;
