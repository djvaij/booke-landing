import Css from "./style.module.scss";

import React, { useCallback, useEffect, useState } from "react";

import { IconAvailable } from "lib/icons";
import { preloadImages } from "utils";
import Layout from "lib/common/Layout";

import RequestForm from "./lib/RequestForm";

const RequestAccess = () => {
  const [formSended, setFormSended] = useState(false);

  const handleFormSend = useCallback(() => {
    setFormSended(true);
  }, []);

  useEffect(() => {
    preloadImages(["/images/ta-da.svg"]);
  }, []);

  return (
    <Layout title="Request an access">
      <div className={Css.requestAccesPage}>
        <div className={Css.container}>
          <div className={Css.left}>
            <div className={Css.list}>
              <div className={Css.item}>
                <IconAvailable />
                <div className={Css.title}>Auto-categorization of transactions</div>
                <div>With 100% certainty we know how you categorise and therefore can do the majority of the work for you!</div>
              </div>
              <div className={Css.item}>
                <IconAvailable />
                <div className={Css.title}>Better call your relatives, not clients</div>
                <div>Because clients hate calls and lots of emails</div>
              </div>
              <div className={Css.item}>
                <IconAvailable />
                <div className={Css.title}>Error-free P&amp;L</div>
                <div>
                  Human error accounts for 41% of inaccurate numbers in reporting.
                  We can help you reduce the errors by more than 90%!
                </div>
              </div>
            </div>
          </div>
          <div className={Css.right}>
            <div className={Css.paper}>
              {formSended
                ? (
                  <div className={Css.congrats}>
                    <div className={Css.image}>
                      <img src="/images/ta-da.svg" />
                    </div>
                    <div className={Css.title}>Congratulations!</div>
                    <div>Your data has been successfully sent. Our managers will contact you soon</div>
                    <a href="/">
                      <button>Go to Homepage</button>
                    </a>
                  </div>
                )
                : (
                  <>
                    <div className={Css.title}>Request an access</div>
                    <RequestForm onFormSend={handleFormSend} />
                  </>
                )}
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default RequestAccess;
