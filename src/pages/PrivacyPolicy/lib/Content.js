/* eslint-disable react/no-unescaped-entities */
/* eslint-disable max-len */
import React from "react";

const Content = () => {
  return (
    <div>
      <h3>PRIVACY POLICY</h3>
      <div>Your privacy is important to us, so Booke AI, has created the following Privacy Policy ("Policy") to let you know what information we collect when you visit our Site https://booke.ai ("Site"), why we collect it and how we use it.</div>
      <div>
        <div>The terms "You," "Your," "Yours" and "User" refer to the entity/person/organization using our Site.</div>
        <div>When this Policy mentions "We", "Us," and "Our" it refers to Booke AI and its subsidiaries and affiliates.</div>
      </div>
      <div>For any questions regarding this Policy or any requests regarding the processing of personal data, please contact us at team@booke.ai.</div>
      <div>
        <h3>1. INFORMATION WE COLLECT FROM YOU</h3>
        <div>We collect the information You provide to us and this information is necessary for the adequate performance of the contractual arrangement which is in place between You and us and allow us to comply with our legal obligations.</div>
        <ul>
          <li>Login information. We collect Login information if You are logging to our account with Authentication Data.</li>
        </ul>
      </div>
      <div>
        <h3>2. INFORMATION WE COLLECT AUTOMATICALLY</h3>
        <div>When you use our Site or contact us directly we may collect information, including your personal information, about the way you act in our Site, the services You use and how You use them.</div>
        <div>This information is necessary for the adequate performance of the contract between You and us, to enable us to comply with legal obligations and given our legitimate interest in being able to provide and improve the functionalities of the Site.</div>
        <ul>
          <li>Log data and Device information. We automatically collect log data and device information when you access and use the Site, even if you have not created an Account or logged in. That information includes, among other things: Internet protocol (IP) addresses, Date/time stamp.</li>
          <li>Tracking technologies and Cookies. We use Cookies. We also automatically collect information about device’s operating system, .</li>
          <li>Usage information. We use a tool called "Google Analytics" to collect information about your interactions with the Site (what pages you visit, such as the pages or content you view, your searches for Listings, bookings you have made, and other actions on the Site. In consequence, Google, Inc. plants a permanent cookie on your web browser to identify you as a unique user the next time you visit this Site). For more information please visit <a href="https://policies.google.com/privacy" target="_blank">Google</a>.</li>
        </ul>
      </div>
      <div>
        <h3>3. THE WAY WE USE YOUR INFORMATION</h3>
        <div>We process your information adhering to the general data processing principles.</div>
        <div>We may use the information we collect through our Site for a number of reasons, including to:</div>
        <ul>
          <li>to create statistics and analyze market</li>
          <li>to customize marketing</li>
          <li>to ensure data security and prevent fraud</li>
        </ul>
        <div>We will normally collect personal information from you only where we have your consent to do so, where we need the personal information to perform a contract with you, or where the processing is in our legitimate business interests.</div>
      </div>
      <div>
        <h3>4. DIRECT MARKETING</h3>
        <div>We may use your provided contact details for direct marketing. These direct marketing offers, depending on your preferences, may be personalized taking into account any other information which you have provided to us (e.g. location, social media profile information, etc.) or we have collected or generated from other sources as described below.</div>
        <div>If you wish to withdraw the consent for direct marketing, and refuse to receive information from us, you may exercise such option at any time you wish by updating your preferences in your account, clicking on a link for that purpose at the bottom of our e-mail with the newsletters.</div>
      </div>
      <div>
        <h3>5. COOKIES</h3>
        <div>Cookies are small text files stored by your browser on your computer when you visit our Site. We use cookies to improve our Site and make it easier to use. Cookies permit us to recognize users and avoid repetitive requests for the same information.</div>
        <div>Please check our <a href="https://booke.ai/cookie-policy/" target="_blank">Cookie Policy</a> to find more information about cookies we use.</div>
        <div>You may find more information about how to delete cookies, as well as the other useful information related to the use of the cookies, on the website http://www.allaboutcookies.org/.</div>
      </div>
      <div>
        <h3>6. SENSITIVE INFORMATION</h3>
        <div>We do not collect sensitive information such as political opinions, religious or philosophical beliefs, racial or ethnic origin, genetic data, biometric data, health data or data related a sexual orientation.</div>
        <div>Please do not send, upload, or provide us any sensitive data and contact us using the contact details below if you believe that we might have such information. We have a right to delete any information we believe it might contain sensitive data.</div>
      </div>
      <div>
        <h3>7. RETENTION</h3>
        <div>We retain your personal information to provide services to you and as otherwise necessary to comply with our legal obligation, resolve disputes, and enforce our agreements.</div>
        <div>We will retain your personal information as long as we need it to provide services to you, unless we are otherwise required by law or regulations to retain your personal information longer.</div>
      </div>
      <div>
        <h3>8. SECURITY</h3>
        <div>We have implemented security measures designed to protect the personal information you share with us, including physical, electronic and procedural measures. Among other things, we regularly monitor our systems for possible vulnerabilities and attacks.</div>
        <div>Regardless of the measures and efforts taken by us, the transmission of information via internet, email or text message is not completely secure. We do not guarantee the absolute protection and security of your personal information or any other User Content you upload, publish or otherwise share with us or anyone else.</div>
        <div>We therefore encourage you to avoid providing us or anyone with any sensitive information of which you believe its disclosure could cause you substantial or irreparable harm.</div>
        <div>If you have any questions regarding the security of our Site or Services, you are welcome to contact us at team@booke.ai.</div>
      </div>
      <div>
        <h3>9. YOUR RIGHTS</h3>
        <div>You are entitled to a range of rights regarding the protection of your personal information. Those rights are:</div>
        <ul>
          <li>The right to access the information we have about you. If you wish to access your personal information that we collect, you can do so at any time by contacting us using the contact details provided below.</li>
          <li>The right to rectify inaccurate information about you. You can correct, update or request deletion of your personal information by contacting us using the contact details provided below.</li>
          <li>The right to object the processing. When we rely on your consent to process your personal information, you may withdraw consent at any time by contacting us using the contact details provided below. This will not affect the lawfulness of processing prior to the withdrawal of your consent.</li>
          <li>The right to lodge a complaint. You can raise questions or complaints to the national Data Protection Agency in your country of residence in the event where your rights may have been infringed. However, we recommend attempting to reach a peaceful resolution of the possible dispute by contacting us first.</li>
          <li>The right to erase any data concerning you. You may demand erasure of data without undue delay for legitimate reasons, e.g. where data is no longer necessary for the purposes it was collected, or where the data has been unlawfully processed.</li>
        </ul>
      </div>
      <div>
        <h3>10. APPLICATION OF POLICY</h3>
        <div>This Policy applies only to the services offered by our Company. Our Policy does not apply to services offered by other companies or individuals, including products or sites that may be displayed to you in search results, sites that may include our services or other sites linked from our Site or Services.</div>
      </div>
      <div>
        <h3>11. AMENDMENTS</h3>
        <div>Our Policy may change from time to time. We will post any Policy changes on our Site and, if the changes are significant, we may consider providing a more explicit notice (including, for certain services, email notification of Policy changes).</div>
      </div>
      <div>
        <h3>12. ACCEPTANCE OF THIS POLICY</h3>
        <div>We assume that all Users of this Site have carefully read this document and agree to its contents. If someone does not agree with this Policy, they should refrain from using our Site. We reserve the right to change our Policy at any time and inform by using the way as indicated in Section 11. Continued use of this Site implies acceptance of the revised Policy.</div>
      </div>
      <div>
        <h3>13. FURTHER INFORMATION</h3>
        <div>If you have any further questions regarding the data we collect, or how we use it, then please feel free to contact us at the details as indicated above.</div>
        <div>Users from EU and EEA can easily contact our European representative: Vadim team@booke.ai</div>
      </div>
    </div>
  );
};

export default Content;
