import React from "react";

import Layout from "lib/common/Layout";

import Content from "./lib/Content";

const PrivacyPolicy = () => {
  return (
    <Layout title="Privacy Policy">
      <div className="terms">
        <div className="container">
          <Content />
        </div>
      </div>
    </Layout>
  );
};

export default PrivacyPolicy;
