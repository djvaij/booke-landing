/* eslint-disable max-len */
import CallToAction from "lib/common/CallToAction";
import Css from "./styles.module.scss";
import FormBlock from "lib/common/FormBlock";
import HeroAnimation from "./lib/HeroAnimation";
import Layout from "lib/common/Layout";
import MainCustomized from "lib/common/MainCustomized";
import React from "react";
import classNames from "classnames";

const ExecutiveSummary = () => {
  return (
    <Layout title="Executive Summary" index>
      <MainCustomized
        title={"Executive\n Summary"}
        description={`Its common truth, the clients adore human-like
          explanation of their P&L. Why don’t you send it to them?`}
        customAnimationBlock={<HeroAnimation />}
        rightClass={classNames(Css.right, Css.right, Css.right)}
        leftClass={classNames(Css.left, Css.left, Css.left)} />
      <CallToAction />
      <FormBlock />
    </Layout>
  );
};

export default ExecutiveSummary;
