import CallToAction from "lib/common/CallToAction";
import Css from "./styles.module.scss";
import Feature from "lib/common/Feature";
import FormBlock from "lib/common/FormBlock";
import HeroAnimation from "./lib/HeroAnimation";
import Layout from "lib/common/Layout";
import MainCustomized from "lib/common/MainCustomized";
import React from "react";
import classNames from "classnames";

const featuresBackgrounds = {
  first: (
    <svg width="559" height="389" viewBox="0 0 559 389" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M191.038 384.205C195.762 387.874 201.881 389.236 207.715 387.917L542.996 312.134C554.151 309.613 560.924 298.268 557.851 287.252L484.477 24.2543C482.114 15.7847 474.496 9.8519 465.706 9.63504L89.9193 0.36438C81.1405 0.147797 73.2472 5.68314 70.4601 14.0107L1.50014 220.054C-1.19559 228.109 1.49007 236.987 8.19813 242.197L191.038 384.205Z"
        fill="#EFF5FF" />
    </svg>
  )
};

const XeroReconciliation = () => {
  return (
    <Layout title="Browser Extension" index>
      <MainCustomized
        title={"Browser\n Extension"}
        description="Save time when doing bookkeeping"
        customAnimationBlock={<HeroAnimation />}
        rightClass={classNames(Css.right, Css.right)}
        leftClass={classNames(Css.left, Css.left)} />
      <div className="container">
        <Feature
          className={Css.feature}
          video={{
            webmUrl: "/video/xero-reconciliation-booke-extension.webm",
            mpegUrl: "/video/xero-reconciliation-booke-extension.mp4"
          }}
          bgImage={featuresBackgrounds.first}
          bgPosition={{
            top: "-33px",
            left: "-33px"
          }}
          title="Booke Extension"
          description={`Add Booke Extension to your Chrome browser in
            seconds and continue bookkeeping like normal. We’ll
            pop up and do the work for you. With one click Booke
            Extension will automatically look for and match Booke-
            categorized transactions.`} />

      </div>
      <CallToAction />
      <FormBlock />
    </Layout>
  );
};

export default XeroReconciliation;
