import React from "react";

import CallToAction from "lib/common/CallToAction";
import Counters from "lib/common/Counters";
import Features from "lib/common/Features";
import FormBlock from "lib/common/FormBlock";
import Infographics from "lib/common/Infographics";
import Layout from "lib/common/Layout";
import Main from "lib/common/Main";
import VideoFeedback from "lib/common/VideoFeedback";

const QuickbooksLearnMore = () => {
  return (
    <Layout title="QuickBooks Learn More">
      <Main />
      <Infographics quickBooksOnly />
      <Features />
      <VideoFeedback />
      <Counters />
      <CallToAction />
      <FormBlock />
    </Layout>
  );
};

export default QuickbooksLearnMore;
