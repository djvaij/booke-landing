import Css from "./style.module.scss";

import React from "react";

import Layout from "lib/common/Layout";

const CustomerStories = ({ stories }) => {
  return (
    <Layout title="Customer stories">
      <div className="container">
        <div className={Css.customerStories}>
          <h1>Customer stories</h1>
          <div className={Css.items}>
            {stories.map((story) => {
              return (
                <div key={story.slug} className={Css.item}>
                  <a href={`/customer-stories/${story.slug}`}>{story.data.title}</a>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default CustomerStories;
