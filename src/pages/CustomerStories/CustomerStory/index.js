import Css from "./style.module.scss";

import React from "react";

import Layout from "lib/common/Layout";

const CustomerStory = ({ story }) => {
  const { data = {}, content } = story || {};

  return (
    <Layout title="Customer stories">
      <div className="container">
        <div className={Css.customerStory}>
          <h1 className={Css.title}>{data.title}</h1>
          <div className={Css.container}>
            <div className={Css.content}>
              {!!data.vimeo && (
                <div className={Css.video}>
                  <iframe
                    src={`https://player.vimeo.com/video/${data.vimeo}?title=0&byline=0&portrait=0`}
                    width="640"
                    height="360"
                    frameBorder="0"
                    allowFullScreen />
                </div>
              )}
              <div dangerouslySetInnerHTML={{ __html: content }} />
            </div>
            <div className={Css.aside}>
              <div className={Css.image}>
                <img src={`/images/${data.image}`} alt={data.name} />
              </div>
              <div className={Css.heading}>
                About {data.name}
              </div>
              <div className={Css.item}>
                <b>Services:</b> {data.services}
              </div>
              <div className={Css.item}>
                <b>Platforms:</b> {data.platforms}
              </div>
              <div className={Css.item}>
                <b>Location:</b> {data.location}
              </div>
              <div className={Css.item}>
                <b>Firm Size:</b> {data.size}
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default CustomerStory;
