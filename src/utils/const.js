export const API_URL = process.env.NEXT_PUBLIC_API_URL;
export const EMAIL = process.env.NEXT_PUBLIC_EMAIL;
export const CALENDLY_WIDGET_URL = "https://calendly.com/vadim-booke-ai/30min";
export const AUTH_URL = "https://auth.booke.ai/?view=login";
export const SIGNUP_URL = "https://auth.booke.ai/?view=__signup";
export const PAGE_DEMO_PATH = "/accounting-automation-demo";
export const PAGE_REQUEST_ACCESS_PATH = "/request-access";
