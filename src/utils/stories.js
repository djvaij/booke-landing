// eslint-disable-next-line import/no-nodejs-modules
import fs from "fs";
import markdown from "markdown-it";
import yamlParser from "gray-matter";

const STORIES_DIR = "src/stories";

export const getStories = () => {
  const files = fs.readdirSync(STORIES_DIR);

  const stories = files.map((fileName) => {
    const slug = fileName.replace(".markdown", "");

    const fileContent = fs.readFileSync(`${STORIES_DIR}/${fileName}`, "utf-8");

    const { data } = yamlParser(fileContent);

    return {
      slug,
      data
    };
  });

  return stories;
};

export const getStory = (slug) => {
  const fileContent = fs.readFileSync(`${STORIES_DIR}/${slug}.markdown`, "utf-8");

  const { data, content } = yamlParser(fileContent);

  return {
    slug,
    data,
    content: markdown("commonmark").render(content)
  };
};
