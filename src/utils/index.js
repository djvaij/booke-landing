export const scriptExists = (src) => {
  try {
    return !!document.head.querySelector(`[src="${src}"]`);
  } catch (exception) {
    return false;
  }
};

export const addScript = (src, { async } = {}, onload) => {
  const script = document.createElement("script");

  script.addEventListener("load", onload);
  script.async = async;
  script.src = src;
  document.head.appendChild(script);
};

export const emptyObject = (object) => !object || !Object.values(object).filter(Boolean).length;

export const preloadImages = (imagesUrls) => {
  return Promise.all(imagesUrls.map((imageUrl) => {
    return new Promise((resolve, reject) => {
      const image = new Image();

      const handleLoadComplete = () => resolve(image);

      image.src = imageUrl;
      if (image.complete || image.readyState === XMLHttpRequest.DONE) handleLoadComplete();
      else {
        image.addEventListener("load", handleLoadComplete);
        image.addEventListener("error", () => reject());
      }
    });
  }));
};

export const initGa = () => {
  /* eslint-disable prefer-rest-params */
  (function(src, gaId, head, script) {
    head = document.getElementsByTagName("head")[0];
    script = document.createElement("script");
    script.async = 1;
    script.src = src + gaId;
    head.appendChild(script);
    window.dataLayer = window.dataLayer || [];
    window.gtag = window.gtag || function() { window.dataLayer.push(arguments); };
    window.gtag("js", new Date());
    window.gtag("config", gaId);
  })("https://www.googletagmanager.com/gtag/js?id=", "G-22H805Y5Y2");
  /* eslint-enable prefer-rest-params */
};

export const initHotjar = () => {
  /* eslint-disable */
  (function(h,o,t,j,a,r){
    h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
    h._hjSettings={hjid:1881377,hjsv:6};
    a=o.getElementsByTagName('head')[0];
    r=o.createElement('script');r.async=1;
    r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
    a.appendChild(r);
  })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
  /* eslint-enable */
};

export const initIntercom = () => {
  /* eslint-disable */
  window.intercomSettings = {
    api_base: "https://api-iam.intercom.io",
    app_id: "cjnkgoqp"
  };
  (function() {
    var w = window; var ic = w.Intercom; if (typeof ic === "function") {
      ic("reattach_activator");
      ic("update", w.intercomSettings);
    } else {
      var d = document; var i = function () { i.c(arguments); }; i.q = [];
      i.c = function (args) { i.q.push(args); }; w.Intercom = i; var l = function () {
        var s = d.createElement("script");
        s.type = "text/javascript";
        s.async = 1;
        s.src = "https://widget.intercom.io/widget/cjnkgoqp";
        var x = d.getElementsByTagName("script")[0]; x.parentNode.insertBefore(s, x);
      };
      if (document.readyState === "complete") { l(); } else if (w.attachEvent) { w.attachEvent("onload", l); }
      else { w.addEventListener("load", l, false); }
    }
  })();
  /* eslint-enable */
};

export const checkIsMobileLayout = () => {
  return "ontouchstart" in window;
};
