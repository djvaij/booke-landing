import { createContext } from "react";

const TextsContext = createContext();

export default TextsContext;
