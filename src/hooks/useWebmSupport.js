import { useEffect, useState } from "react";

const useWebmSupport = () => {
  const [webmSupported, setWebmSupport] = useState(false);

  useEffect(() => {
    const testEl = document.createElement("video");

    const result = testEl.canPlayType("video/webm; codecs=\"vp8, vorbis\"") !== "";

    testEl.remove();

    setWebmSupport(result);
  }, []);

  return {
    webmSupported
  };
};

export default useWebmSupport;
