import { useCallback, useEffect, useState } from "react";

const useMediaQuery = (query) => {
  const [matches, setMatches] = useState(null);

  const handleChange = useCallback(() => {
    setMatches(window.matchMedia(query).matches);
  }, [query]);

  useEffect(() => {
    const matchMedia = window.matchMedia(query);

    handleChange();

    if (!matchMedia.addEventListener) {
      return () => {};
    }

    matchMedia.addEventListener("change", handleChange);

    return () => {
      matchMedia.removeEventListener("change", handleChange);
    };
  }, [handleChange, query]);

  return matches;
};

export default useMediaQuery;
