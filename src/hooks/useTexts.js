import { useContext } from "react";

import TextsContext from "cntx/TextsContext";

const useTexts = () => {
  return useContext(TextsContext);
};

export default useTexts;
