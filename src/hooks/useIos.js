import { useEffect, useState } from "react";

const checkIos = () => {
  return [
    "iPad Simulator",
    "iPhone Simulator",
    "iPod Simulator",
    "iPad",
    "iPhone",
    "iPod"
  ].includes(navigator.platform)
  // IPad on iOS 13 detection
  || (navigator.userAgent.includes("Mac") && "ontouchend" in document);
};

const useIos = () => {
  const [iosPlatform, setIosPlatform] = useState(false);

  useEffect(() => {
    const result = checkIos();

    if (result) {
      setIosPlatform(true);
    }
  }, []);

  return {
    iosPlatform
  };
};

export default useIos;
