import { useEffect, useState } from "react";

const useCheckSafariBrowser = () => {
  const [safariBrowser, setSafariBrowser] = useState(false);

  useEffect(() => {
    const ua = navigator.userAgent.toLowerCase();

    if (ua.includes("safari")) {
      if (ua.includes("chrome")) {
        setSafariBrowser(false); // Chrome
      } else {
        setSafariBrowser(true); // Safari
      }
    }
  }, []);

  return {
    safariBrowser
  };
};

export default useCheckSafariBrowser;
