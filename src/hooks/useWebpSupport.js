import { useEffect, useState } from "react";

const useWebpSupport = () => {
  const [webpSupported, setWebpSupport] = useState(false);

  const setToSupported = () => {
    setWebpSupport(true);
  };

  const setToUnsupported = () => {
    setWebpSupport(false);
  };

  useEffect(() => {
    let image = new Image();
  
    image.addEventListener("error", setToUnsupported);
    image.addEventListener("load", setToSupported);
    image.src = "data:image/webp;base64,UklGRiQAAABXRUJQVlA4IBgAAAAwAQCdASoBAAEAAwA0JaQAA3AA/vuUAAA=";

    return () => {
      image.removeEventListener("error", setToSupported);
      image.removeEventListener("load", setToUnsupported);
      image = null;
    };
  }, []);

  return {
    webpSupported
  };
};

export default useWebpSupport;
