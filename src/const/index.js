// Dynamic pages revalidate interval in seconds
export const REVALIDATE_INTERVAL = 3600;

export const POSTS_PER_PAGE = 12;
