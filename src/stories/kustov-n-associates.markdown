---
title: Kustov & Associates is more than just your accountant, it’s your competitive edge!
name: Kustov & Associates
services: Financial Statements Reviews, Audits, Tax, Bookkeeping, Accounting, Advisory
platforms: Xero, QuickBooks Online, QuickBooks Desktop
location: San Francisco, CA, United States
size: 2-10 Employees
image: kustov.png
vimeo: 691381800
---

Kustov and Associates team understand that communicating with clients it’s inevitable part of Bookkeeper’s workflow. And the most time-consuming:

> When Bookkeeper asks about transactions from a client, even with today’s emails and spreadsheets, it still takes a lot of time
> <figcaption>Dmitry Kustov, CPA and CEO of Kustov & Associates</figcaption>

Dmitriy also shared some other insights and thoughts about pain points associated with client’s communication:

> It’s not enough just to record the transaction. All the decisions must be supported by written evidence
> <figcaption>CEO of Kustov & Associates</figcaption>

Being committed to ensuring his team members and clients are happy, he finds a technology partner like Booke AI that helps to eliminate 80% of the bookkeeper’s daily routine and spreadsheet-dance with the clients, a perfect fit he was looking for:

> With Booke it’s… BAAAAM!
> All of the steps are done at once,
> all are saved in one place for easy access in the future.
> We use it everyday in our CPA practice. It’s very efficient
> <figcaption>CEO of Kustov & Associates</figcaption>