import Css from "./style.module.scss";

import React from "react";

import Document from "./lib/Document";
import Footer from "./lib/Footer";
import Header from "./lib/Header";

const Layout = ({ index, children, ...restProps }) => {
  return (
    <Document {...restProps}>
      <div id="main" />
      <Header index={index} />
      <div className={Css.layout}>
        {children}
        <Footer />
      </div>
    </Document>
  );
};

export default Layout;
