import Head from "next/head";
import React, { useEffect, useState } from "react";

import { initGa, initHotjar, initIntercom } from "utils";
import Dialogs from "lib/common/Dialogs";
import Spiner from "lib/common/Spiner";
import useTexts from "hooks/useTexts";

const COMPANY_NAME = "Booke AI";

const DEFAULT_TITLE = "For Accountants";

const scrollToTarget = () => {
  if (!location.hash) return;

  const targetElement = document.getElementById(location.hash.slice(1));

  if (!targetElement || !targetElement.scrollIntoView) return;

  targetElement.scrollIntoView();
};

const Document = (props) => {
  const texts = useTexts();

  const [oreloaderShown, setPreloaderShown] = useState(true);

  const {
    seo = {},
    showPreloader,
    title = DEFAULT_TITLE,
    description = texts.homePageDescription,
    children
  } = props;

  const seoTitle = `${COMPANY_NAME} – ${title}`;

  useEffect(() => {
    scrollToTarget();

    if (showPreloader) return;

    const preloader = document.getElementById("preloader");

    if (!preloader) return;

    setTimeout(() => {
      setPreloaderShown(false);
    // eslint-disable-next-line no-magic-numbers
    }, 600);
  }, [showPreloader]);

  useEffect(() => {
    if (!localStorage.referrer) {
      localStorage.referrer = document.referrer;
    }
    window.addEventListener("load", () => {
      initGa();
      initHotjar();
      initIntercom();
    });
  }, []);

  return (
    <>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <meta name="msapplication-TileColor" content="#2b5797" />
        <meta name="theme-color" content="#003166" />

        <title>{seoTitle}</title>
        <meta name="title" content={seoTitle} />
        <meta name="description" content={description} />
        <meta name="keywords" content="accounting, reconciliation, ocr, automation" />

        <meta httpEquiv="content-language" content="en-US" />

        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://booke.ai" />
        <meta property="og:title" content={seo.og_title || seoTitle} />
        <meta property="og:description" content={seo.og_description || description} />
        <meta property="og:site_name" content="Booke AI" />
        <meta property="og:image:alt" content="Booke AI" />
        <meta property="og:image" content={seo.og_image || "https://booke.ai/images/preview.png"} />

        <meta property="twitter:title" content={seo.twitter_title || seoTitle} />
        <meta property="twitter:description" content={seo.twitter_description || description} />
        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:url" content="https://booke.ai" />
        <meta property="twitter:image" content={seo.twitter_image || "https://booke.ai/images/preview.png"} />
      </Head>
      {children}
      {oreloaderShown && (<div id="preloader"><Spiner /></div>)}
      <Dialogs />
      <script
        src="https://app.termshub.io/68ce7580-4b34-4bf0-9c17-e08fef01bb97/termshub-cookie-consent.js"
        site="Booke_AI" defer />
    </>
  );
};

export default Document;
