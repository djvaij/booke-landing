import Css from "./style.module.scss";

import React from "react";

import { AUTH_URL, CALENDLY_WIDGET_URL } from "utils/const";

const Footer = () => {
  return (
    <div className={Css.footer}>
      <div className="container">
        <div className={Css.main}>
          <div className={Css.logo}>
            <img src="/images/logo-white.svg" />
          </div>
          <div className="row-m">
            <div className={Css.colFirst}>
              <div className={Css.textLarge}>We love doing boring accounting work, so you don’t</div>
            </div>
            <div className={Css.colSecond}>
              <div className={Css.title}>Curious how it works?</div>
              <a href={CALENDLY_WIDGET_URL} className={`button block ${Css.button}`}>Request a Demo</a>
            </div>
            <div className={Css.colThird}>
              <div className={Css.title}>Already have an account?</div>
              <a href={AUTH_URL} className={`button stroke block ${Css.button}`}>Sign In</a>
            </div>
          </div>
        </div>
        <div className={`row-m ${Css.sub}`}>
          <div className={`col ${Css.left}`}>
            <a href="/terms-and-conditions">Terms and Conditions</a>
            <a href="/privacy-policy">Privacy Policy</a>
            <a href="/cookie-policy">Cookie Policy</a>
            <a href="/hub">Resource Hub</a>
          </div>
          <div className={`col ${Css.center}`}>
            <span>Backed by</span>
            <img src="/images/alchemist_soft.png" />
          </div>
          <div className={`col ${Css.right}`}>
            {`Booke AI Inc. © ${new Date().getFullYear()} All rights reserved`}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
