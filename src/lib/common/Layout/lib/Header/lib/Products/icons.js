import React from "react";

const icons = {
  "Auto-categorization": (
    <svg
      width={44}
      height={44}
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <rect width={44} height={44} rx={22} fill="#FFFDF1" />
      <path
        d="M26.4 20.24h1.761c1.76 0 2.64-.88 2.64-2.641v-1.76c0-1.76-.88-2.641-2.64-2.641h-1.76c-1.76 0-2.64.88-2.64 2.64V17.6c0 1.76.88 2.64 2.64 2.64ZM15.838 30.802h1.76c1.761 0 2.641-.88 2.641-2.64v-1.76c0-1.761-.88-2.642-2.64-2.642h-1.76c-1.761 0-2.641.88-2.641 2.641v1.76c0 1.761.88 2.641 2.64 2.641ZM16.719 20.24a3.52 3.52 0 1 0 0-7.042 3.52 3.52 0 0 0 0 7.042ZM27.281 30.802a3.52 3.52 0 1 0 0-7.042 3.52 3.52 0 0 0 0 7.042Z"
        stroke="#F3D434"
        strokeWidth={1.5}
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round" />
    </svg>
  ),

  "Two-way integrations": (
    <svg
      width={44}
      height={44}
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <rect width={44} height={44} rx={22} fill="#F4F9FF" />
      <path
        d="M11.166 25.25a7.578 7.578 0 0 0 7.584 7.583l-1.138-1.895M32.833 18.75a7.578 7.578 0 0 0-7.583-7.583l1.137 1.896"
        stroke="#0267D2"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round" />
      <path
        d="M21.996 23.581a1.95 1.95 0 1 0 0-3.898 1.95 1.95 0 0 0 0 3.898Z"
        stroke="#0267D2"
        strokeWidth={1.5}
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round" />
      <path
        d="M15.5 22.204v-1.143c0-.676.552-1.235 1.234-1.235 1.176 0 1.657-.831 1.066-1.851a1.234 1.234 0 0 1 .455-1.683l1.124-.643a1.084 1.084 0 0 1 1.48.39l.072.123c.585 1.02 1.547 1.02 2.138 0l.071-.123a1.084 1.084 0 0 1 1.482-.39l1.123.643c.592.338.793 1.098.455 1.683-.59 1.02-.11 1.851 1.066 1.851.675 0 1.234.552 1.234 1.235v1.143c0 .676-.552 1.234-1.234 1.234-1.176 0-1.657.832-1.066 1.852a1.232 1.232 0 0 1-.454 1.683l-1.124.643a1.084 1.084 0 0 1-1.482-.39l-.071-.123c-.585-1.02-1.546-1.02-2.138 0l-.071.123a1.084 1.084 0 0 1-1.481.39l-1.124-.643a1.234 1.234 0 0 1-.455-1.683c.591-1.02.11-1.852-1.066-1.852a1.238 1.238 0 0 1-1.234-1.234Z"
        stroke="#0267D2"
        strokeWidth={1.5}
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round" />
    </svg>
  ),

  "Invoice & Receipt OCR AI": (
    <svg
      width={44}
      height={44}
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <rect width={44} height={44} rx={22} fill="#FFF7F8" />
      <path
        d="M32.834 15.5v2.622c0 1.711-1.084 2.795-2.796 2.795h-3.704v-7.573c0-1.202.985-2.177 2.188-2.177 1.18.01 2.264.487 3.044 1.267a4.36 4.36 0 0 1 1.268 3.066Z"
        stroke="#F3545E"
        strokeWidth={1.5}
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round" />
      <path
        d="M11.166 16.583V31.75a1.08 1.08 0 0 0 1.734.867l1.852-1.387a1.091 1.091 0 0 1 1.43.108l1.799 1.81a1.092 1.092 0 0 0 1.538 0l1.82-1.82c.38-.38.986-.423 1.408-.098l1.853 1.387c.715.53 1.733.021 1.733-.867V13.333c0-1.191.975-2.166 2.167-2.166h-13c-3.25 0-4.334 1.939-4.334 4.333v1.083Z"
        stroke="#F3545E"
        strokeWidth={1.5}
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round" />
      <path
        d="M18.75 23.094H22M18.75 18.76H22"
        stroke="#F3545E"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round" />
      <path
        d="M15.495 23.083h.01M15.495 18.75h.01"
        stroke="#F3545E"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round" />
    </svg>
  ),

  "Intelligent AI-assistant": (
    <svg
      width={44}
      height={44}
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <rect width={44} height={44} rx={22} fill="#FFF7F4" />
      <path
        d="M19.4 30.667h5.2c4.333 0 6.067-1.734 6.067-6.067v-5.2c0-4.333-1.734-6.067-6.067-6.067h-5.2c-4.333 0-6.066 1.734-6.066 6.067v5.2c0 4.333 1.733 6.067 6.066 6.067ZM17.678 13.333v-2.166M22 13.333v-2.166M26.334 13.333v-2.166M30.666 17.667h2.167M30.666 22h2.167M30.666 26.333h2.167M26.334 30.667v2.166M22.01 30.667v2.166M17.678 30.667v2.166M11.166 17.667h2.167M11.166 22h2.167M11.166 26.333h2.167"
        stroke="#ED6135"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round" />
      <path
        d="m18.061 25.856 2.305-7.375a.1.1 0 0 1 .19 0l2.305 7.375"
        stroke="#ED6135"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="bevel" />
      <path
        d="M21.901 22.976h-2.88"
        stroke="#ED6135"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round" />
      <path
        stroke="#ED6135"
        strokeWidth={1.5}
        strokeLinecap="round"
        d="M25.508 18.449v7.38" />
    </svg>
  ),

  Communication: (
    <svg
      width={44}
      height={44}
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <rect width={44} height={44} rx={22} fill="#F9F5FE" />
      <path
        d="m27.695 26.252.343 2.78c.088.732-.695 1.242-1.32.863l-3.688-2.191c-.405 0-.8-.027-1.188-.08a4.279 4.279 0 0 0 1.038-2.78c0-2.5-2.165-4.525-4.84-4.525-1.022 0-1.964.29-2.747.8a5.581 5.581 0 0 1-.035-.668c0-4.005 3.477-7.253 7.772-7.253 4.296 0 7.772 3.248 7.772 7.253 0 2.377-1.223 4.48-3.107 5.8Z"
        stroke="#9550ED"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round" />
      <path
        d="M22.88 24.843a4.279 4.279 0 0 1-1.039 2.782c-.871 1.056-2.253 1.734-3.802 1.734l-2.297 1.364c-.388.238-.88-.088-.828-.537l.22-1.734c-1.18-.819-1.936-2.13-1.936-3.609 0-1.55.827-2.913 2.095-3.723a5.002 5.002 0 0 1 2.746-.801c2.676 0 4.841 2.024 4.841 4.524Z"
        stroke="#9550ED"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round" />
    </svg>
  ),

  "Accounts Payable workflow": (
    <svg
      width={44}
      height={44}
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <rect width={44} height={44} rx={22} fill="#F2FFF9" />
      <path
        d="M29.583 26.333V16.042a2.173 2.173 0 0 0-2.166-2.167H22.54"
        stroke="#1DB173"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round" />
      <path
        d="M25.25 11.167 22 13.875l3.25 2.708M14.416 18.75v7.583M14.687 18.208a3.52 3.52 0 1 0 0-7.041 3.52 3.52 0 0 0 0 7.041ZM14.416 32.833a3.25 3.25 0 1 0 0-6.5 3.25 3.25 0 0 0 0 6.5ZM29.584 32.833a3.25 3.25 0 1 0 0-6.5 3.25 3.25 0 0 0 0 6.5Z"
        stroke="#1DB173"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round" />
    </svg>
  ),
  "Inconsistencies and error detection": (
    <svg
      width={44}
      height={44}
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <rect width={44} height={44} rx={22} fill="#FFF7F8" />
      <path
        d="M22 19.563v4.062M20.594 13.06l-8.936 15.438a1.626 1.626 0 0 0 1.407 2.439h17.87a1.625 1.625 0 0 0 1.407-2.44l-8.936-15.436a1.624 1.624 0 0 0-2.812 0v0Z"
        stroke="#F3545E"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round" />
      <path
        d="M22 28.5a1.219 1.219 0 1 0 0-2.438 1.219 1.219 0 0 0 0 2.438Z"
        fill="#F3545E" />
    </svg>
  ),
  "Trend Analysis": (
    <svg
      width={44}
      height={44}
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <rect width={44} height={44} rx={22} fill="#F2FFF9" />
      <path
        d="M18.718 32.833h6.5c5.416 0 7.583-2.166 7.583-7.583v-6.5c0-5.417-2.167-7.583-7.583-7.583h-6.5c-5.417 0-7.584 2.166-7.584 7.583v6.5c0 5.417 2.167 7.583 7.584 7.583Z"
        stroke="#1DB173"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round" />
      <path
        d="m11.134 22.758 6.5-.021c.813 0 1.723.617 2.026 1.376l1.235 3.12c.282.704.726.704 1.008 0l2.48-6.295c.239-.606.683-.628.986-.054l1.127 2.134c.336.64 1.202 1.16 1.917 1.16h4.399"
        stroke="#1DB173"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round" />
    </svg>
  ),
  "Bulk reclassification": (
    <svg
      width={44}
      height={44}
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <rect width={44} height={44} rx={22} fill="#FFFDEF" />
      <path
        d="M20.916 30.125H31.75M20.916 22.542H31.75M20.916 14.958H31.75M12.25 14.958l1.083 1.084 3.25-3.25M12.25 22.542l1.083 1.083 3.25-3.25M12.25 30.125l1.083 1.083 3.25-3.25"
        stroke="#F3D434"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round" />
    </svg>
  ),
  "Xero reconciliation": (
    <svg
      width={44}
      height={44}
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <rect width={44} height={44} rx={22} fill="#F1FCFF" />
      <path
        d="M16.494 21.221a3.257 3.257 0 0 1 3.161-2.445c1.501 0 2.794 1.003 3.165 2.445h-6.326Zm7.474.875c.212-.26.291-.6.224-.958-.276-1.298-.983-2.337-2.048-3.007a4.6 4.6 0 0 0-2.466-.707c-.974 0-1.902.292-2.682.845a4.631 4.631 0 0 0-1.948 3.775c0 .377.046.75.139 1.111a4.659 4.659 0 0 0 3.923 3.439 4.399 4.399 0 0 0 1.659-.113c.49-.117.956-.314 1.386-.586.407-.262.781-.616 1.175-1.11l.026-.026a.773.773 0 0 0 .167-.575.642.642 0 0 0-.245-.44.715.715 0 0 0-.43-.152c-.146 0-.36.053-.552.303l-.016.02c-.064.085-.13.173-.205.26-.26.29-.56.53-.889.712-.471.25-.98.38-1.512.383-1.67-.018-2.693-1.125-3.092-2.153a3.676 3.676 0 0 1-.137-.511l-.003-.049 6.467-.001c.449-.01.825-.173 1.059-.46ZM33.391 20.795c-.65 0-1.177.525-1.177 1.17 0 .644.528 1.17 1.177 1.17.648 0 1.175-.526 1.175-1.17 0-.645-.527-1.17-1.175-1.17ZM29.102 18.158a.67.67 0 0 0-.672-.666l-.189-.003c-.573 0-1.116.175-1.572.508a.667.667 0 0 0-1.3.203l.003 7.674a.665.665 0 0 0 1.332 0v-4.72c0-1.529.132-2.168 1.463-2.332.11-.013.228-.014.259-.014.391-.014.676-.287.676-.65Z"
        fill="#13B5EA" />
      <path
        d="m11.554 22.014 3.45-3.447a.648.648 0 0 0 .196-.467.669.669 0 0 0-1.145-.466l-3.451 3.43-3.465-3.437A.67.67 0 0 0 6 18.1c0 .178.071.347.201.475l3.453 3.436-3.448 3.44A.654.654 0 0 0 6 25.93c0 .367.3.665.667.665a.68.68 0 0 0 .472-.192L10.6 22.96l3.446 3.427a.67.67 0 0 0 1.154-.457.662.662 0 0 0-.195-.469l-3.451-3.446ZM33.39 25.18a3.228 3.228 0 0 1-3.233-3.216 3.23 3.23 0 0 1 3.233-3.22c1.78 0 3.23 1.445 3.23 3.22a3.227 3.227 0 0 1-3.23 3.217Zm0-7.806c-2.542 0-4.61 2.059-4.61 4.59 0 2.53 2.068 4.59 4.61 4.59 2.542 0 4.61-2.06 4.61-4.59 0-2.531-2.068-4.59-4.61-4.59Z"
        fill="#13B5EA" />
    </svg>
  ),
  "QBO reconciliation": (
    <svg
      width={44}
      height={44}
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <rect width={44} height={44} rx={22} fill="#EFFFED" />
      <path
        d="M11.083 27.02a7.085 7.085 0 0 0 5.028 2.097h.999v-2.824h-1v.002a4.294 4.294 0 0 1-3.046-1.27 4.356 4.356 0 0 1-1.263-3.07c0-1.151.454-2.255 1.262-3.07a4.294 4.294 0 0 1 3.048-1.27v.002h2.1V31.07a2.92 2.92 0 0 0 2.91 2.93V14.793h-5.01a7.085 7.085 0 0 0-5.028 2.098A7.188 7.188 0 0 0 9 21.955c0 1.9.75 3.721 2.083 5.064ZM32.917 16.98a7.085 7.085 0 0 0-5.028-2.097h-.999v2.824h1v-.002c1.142 0 2.238.457 3.046 1.27a4.356 4.356 0 0 1 1.263 3.07 4.356 4.356 0 0 1-1.262 3.07 4.294 4.294 0 0 1-3.048 1.27v-.002h-2.1V12.93a2.92 2.92 0 0 0-2.91-2.93v19.207h5.01a7.085 7.085 0 0 0 5.028-2.098A7.188 7.188 0 0 0 35 22.045c0-1.9-.75-3.721-2.083-5.064Z"
        fill="#2C9F1C" />
    </svg>
  ),
  Audit: (
    <svg
      width={44}
      height={44}
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <rect width={44} height={44} rx={22} fill="#F9F5FE" />
      <path
        d="M31.208 21.242v-4.615c0-4.366-1.018-5.46-5.113-5.46h-8.19c-4.095 0-5.114 1.094-5.114 5.46v12.198c0 2.882 1.582 3.564 3.5 1.506l.01-.01c.889-.943 2.243-.868 3.012.162l1.094 1.462"
        stroke="#9550ED"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round" />
      <path
        d="M28.717 32.183a3.467 3.467 0 1 0 0-6.933 3.467 3.467 0 0 0 0 6.933ZM32.833 32.833 31.75 31.75M17.666 16.583h8.667M18.75 20.917h6.5"
        stroke="#9550ED"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round" />
    </svg>
  ),
  Tasks: (
    <svg
      width={44}
      height={44}
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <rect width={44} height={44} rx={22} fill="#ECFCF5" />
      <path
        d="m19.632 24.377 1.32 1.32 3.521-3.521"
        stroke="#1DB173"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round" />
      <path
        d="M20.24 16.719h3.52c1.76 0 1.76-.88 1.76-1.76 0-1.761-.88-1.761-1.76-1.761h-3.52c-.88 0-1.761 0-1.761 1.76s.88 1.76 1.76 1.76Z"
        stroke="#1DB173"
        strokeWidth={1.5}
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round" />
      <path
        d="M25.52 14.976c2.932.158 4.402 1.24 4.402 5.263v5.282c0 3.52-.88 5.281-5.281 5.281h-5.282c-4.4 0-5.28-1.76-5.28-5.281v-5.282c0-4.013 1.47-5.105 4.4-5.263"
        stroke="#1DB173"
        strokeWidth={1.5}
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round" />
    </svg>
  ),
  "Changes Logging": (
    <svg
      width={44}
      height={44}
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <rect width={44} height={44} rx={22} fill="#F4F9FF" />
      <path
        d="M17.666 11.167v3.25M26.334 11.167v3.25M16.584 23.083h8.666M16.584 27.417H22M26.333 12.792c3.608.195 5.417 1.57 5.417 6.662v6.695c0 4.464-1.083 6.695-6.5 6.695h-6.5c-5.417 0-6.5-2.231-6.5-6.695v-6.695c0-5.091 1.81-6.456 5.417-6.662h8.666Z"
        stroke="#0267D2"
        strokeWidth={1.5}
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round" />
    </svg>
  ),
  "Performance Dashboard": (
    <svg
      width={44}
      height={44}
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <rect width={44} height={44} rx={22} fill="#FFF6F8" />
      <path
        d="M11.166 32.833h21.667"
        stroke="#F3545E"
        strokeWidth={1.5}
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round" />
      <path
        d="M19.563 13.333v19.5h4.875v-19.5c0-1.191-.488-2.166-1.95-2.166h-.976c-1.462 0-1.95.975-1.95 2.166ZM12.25 19.833v13h4.333v-13c0-1.191-.433-2.166-1.733-2.166h-.867c-1.3 0-1.733.975-1.733 2.166ZM27.416 25.25v7.583h4.334V25.25c0-1.192-.433-2.167-1.733-2.167h-.867c-1.3 0-1.733.975-1.733 2.167Z"
        stroke="#F3545E"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round" />
    </svg>
  )
};

export default icons;
