import { Category, Col, Container, Item, Row } from "../PopupMenu";
import Css from "./style.module.scss";
import React from "react";
import classNames from "classnames";
import icons from "./icons";

const Products = ({ className, containerClass }) => {
  return (
    <Container className={containerClass}>
      <div className={classNames(Css.wrapper, className)}>
        <Row>
          <Category title="Uncategorized features">
            <Col>
              <Item
                icon={icons["Auto-categorization"]}
                title="Auto-categorization"
                description="Categorize transactions x5 faster"
                url="/auto-categorization" />
              <Item
                icon={icons["Intelligent AI-assistant"]}
                title="Intelligent AI-assistant"
                description={"Who checks the history of your reconciliation and suggests you \n the best options"}
                url="/intelligent-ai-assistant" />
            </Col>
            <Col>
              <Item
                icon={icons["Two-way integrations"]}
                title="Two-way integrations"
                description="Seamlessly connect to Xero, QBO or QBD"
                url="/two-way-integrations" />
              <Item
                icon={icons.Communication}
                title="Communication"
                description={"Ask your client about transactions \n in one click"}
                url="/communication" />
            </Col>
            <Col>
              <Item
                icon={icons["Invoice & Receipt OCR AI"]}
                title="Invoice & Receipt OCR AI"
                description="Real-time document data extraction"
                url="/invoice-and-receipt-ocr-ai" />
              <Item
                icon={icons["Accounts Payable workflow"]}
                title="Accounts Payable workflow"
                description="Import and manage documents easy and in bulk"
                url="/accounts-payable-workflow" />
            </Col>
          </Category>
        </Row>
        <Row>
          <Category title="Audit features">
            <Col>
              <Item
                icon={icons["Inconsistencies and error detection"]}
                title="Inconsistencies and error detection"
                description="Spot discrepancies in your categorization"
                url="/inconsistencies-and-error-detection" />
              <Item
                disabled
                icon={icons["Trend Analysis"]}
                title="Trend Analysis"
                description={"Rely on Booke AI to deliver \n more quality P&L"}
                url="#" />
              <Item
                icon={icons["Bulk reclassification"]}
                title="Executive summary"
                description={"Reclassify multiple transactions \n in accordance with the set criteria"}
                url="/executive-summary" />
            </Col>
          </Category>
          <Category title="Booke Chrome Extension">
            <Col>
              <Item
                twoLinesDescription
                icon={icons["Xero reconciliation"]}
                title="Xero reconciliation"
                description="Save time when doing bookkeeping"
                url="/xero-reconciliation" />
              <Item
                disabled
                icon={icons["QBO reconciliation"]}
                title="QBO reconciliation"
                description={"Rely on Booke AI to deliver more \n quality P&L"}
                url="#" />
              <Item
                disabled
                icon={icons.Audit}
                title="Audit"
                description={"Reclassify multiple transactions in \n accordance with the set criteria"}
                url="#" />
            </Col>
          </Category>
          <Category title="General features">
            <Col>
              <Item
                icon={icons.Tasks}
                title="Tasks"
                description={"Reduce internal and external \n communication by creating tasks"}
                url="/tasks" />
              <Item
                icon={icons["Changes Logging"]}
                title="Changes Logging"
                description={"We log all the changes so that you don’t \n have to"}
                url="/changes-logging" />
              <Item
                icon={icons["Performance Dashboard"]}
                title="Performance Dashboard"
                description={"Manage and check the progress of all \n your clients in one place"}
                url="/performance-dashboard" />
            </Col>
          </Category>
        </Row>
      </div>
    </Container>
  );
};

export default Products;
