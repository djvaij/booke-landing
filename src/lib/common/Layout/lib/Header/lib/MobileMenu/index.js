import Category from "./lib/Category";
import Css from "./styles.module.scss";
import Item from "./lib/Item";
import React from "react";
import ToggleItem from "./lib/ToggleItem";

const MobileMenu = ({ handleMenuClick }) => {
  return (
    <nav className={Css.nav}>
      <ToggleItem title="Products">
        <Category title="Uncategorized features">
          <Item title="Auto-categorization" url="/auto-categorization" />
          <Item title="Intelligent AI-assistant" url="/intelligent-ai-assistant" />
          <Item title="Two-way integrations" url="/two-way-integrations" />
          <Item title="Communication" url="/communication" />
          <Item title="Invoice & Receipt OCR AI" url="/invoice-and-receipt-ocr-ai" />
          <Item title="Accounts Payable workflow" url="/accounts-payable-workflow" />
        </Category>
        <Category title="Audit features">
          <Item title="Inconsistencies and error detection" url="/inconsistencies-and-error-detection" />
          <Item
            disabled
            title="Trend Analysis"
            url="#" />
          <Item
            disabled
            title="Bulk reclassification"
            url="#" />
        </Category>
        <Category title="Booke Chrome Extension">
          <Item title="Xero reconciliation" url="/xero-reconciliation" />
          <Item disabled title="QBO reconciliation" url="#" />
          <Item disabled title="Audit" url="#" />
        </Category>
        <Category title="General features">
          <Item title="Executive Summary" url="/executive-summary" />
          <Item title="Tasks" url="/tasks" />
          <Item title="Changes Logging" url="/changes-logging" />
          <Item title="Performance Dashboard" url="/performance-dashboard" />
        </Category>
      </ToggleItem>
      <ul>
        <Item onClick={handleMenuClick} title="Use cases" url="/#features" bold />
        <Item onClick={handleMenuClick} title="Prices" url="/#prices" bold />
      </ul>
    </nav>
  );
};

export default MobileMenu;
