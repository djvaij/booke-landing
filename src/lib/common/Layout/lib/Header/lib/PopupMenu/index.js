import Css from "./style.module.scss";
import React from "react";
import classNames from "classnames";

export const Container = ({ children, className }) => {
  return (
    <div className={classNames(Css.container, className)}>
      {children}
    </div>
  );
};

export const Row = ({ children, className }) => {
  return (
    <div className={classNames(Css.row, className)}>
      {children}
    </div>
  );
};

export const Col = ({ className, children }) => {
  return (
    <div className={classNames(Css.col, className)}>
      {children}
    </div>
  );
};

export const Category = ({ title, children, className }) => {
  return (
    <div className={classNames(Css.category, className)}>
      <div className={Css.categoryTitle}>
        {title}
      </div>
      <div className={Css.items}>
        {children}
      </div>
    </div>
  );
};

export const Item = ({ icon, title, description, url, className, twoLinesDescription, disabled }) => {
  return (
    <div className={classNames(Css.item, className)}>
      <a href={url} className={Css.itemIconWrapper}>
        {icon}
      </a>
      <div className={Css.itemTextWrapper}>
        <a className={classNames(Css.itemTitle, { [Css.disabled]: disabled })} href={url}>{title}</a>
        <div
          className={classNames(Css.itemDescription, { [Css.isDescriptionTwoLines]: twoLinesDescription })}>
          {description}
        </div>
      </div>
    </div>
  );
};

export default {
  Container,
  Row,
  Category,
  Item
};
