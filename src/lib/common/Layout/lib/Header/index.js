import Css from "./style.module.scss";

import React, { useCallback, useEffect, useState } from "react";
import classNames from "classnames";

import { AUTH_URL, PAGE_REQUEST_ACCESS_PATH } from "utils/const";
import { IconBurger, IconClose, IconLogIn } from "lib/icons";

import MobileMenu from "./lib/MobileMenu";
import Products from "./lib/Products";

const MobileHeader = ({ opened, onClick }) => {
  return (
    <div className={Css.mobileMenu}>
      <div className={Css.menu} onClick={onClick}>
        {opened ? <IconClose /> : <IconBurger />}
      </div>
      <div className={Css.logo}>
        <a href="/" className={Css.logo}>
          <img src="/images/logo-white.svg" />
        </a>
      </div>
      <a className={Css.login} href={AUTH_URL}>
        Sign In
        <IconLogIn />
      </a>
    </div>
  );
};

const Header = ({ index }) => {
  const [detached, setDetached] = useState(false);

  const [menuOpened, setMenuOpened] = useState(false);

  const handleWindowScroll = useCallback(() => {
    setDetached(window.scrollY > 0);
  }, []);

  const handleMenuClick = useCallback(() => {
    setMenuOpened((prev) => !prev);
  }, []);

  useEffect(() => {
    window.addEventListener("scroll", handleWindowScroll);
    handleWindowScroll();

    return () => {
      window.removeEventListener("scroll", handleWindowScroll);
    };
  }, [handleWindowScroll]);

  useEffect(() => {
    if (menuOpened) {
      document.documentElement.classList.add("overflow-hidden");
    } else {
      document.documentElement.classList.remove("overflow-hidden");
    }
  }, [menuOpened]);

  return (
    <>
      {menuOpened && (
        <div className={Css.offCanvas}>
          <MobileHeader opened={menuOpened} />
          <MobileMenu handleMenuClick={handleMenuClick} />
          <div className={Css.overlay} onClick={handleMenuClick} />
        </div>
      )}
      <div
        className={classNames(
          Css.header,
          index && Css.index,
          detached && Css.detached
        )}>
        <div className={`container ${Css.container}`}>
          <div className={Css.inner}>
            <div className={Css.left}>
              <a href="/" className={Css.logo}>
                <img src="/images/logo.svg" alt="Booke.ai" />
              </a>
              <nav>
                <span className={Css.popupItem}>
                  Products
                  <Products containerClass={classNames({ [Css.scrolledPopupMenu]: detached }, Css.popupMenu)} />
                </span>
                <a href="/#features">Use cases</a>
                <a href="/#prices">Prices</a>
              </nav>
            </div>
            <div className={Css.right}>
              <a href={AUTH_URL}>Sign in</a>
              <a href={PAGE_REQUEST_ACCESS_PATH} className={Css.bordered}>
                Start a Trial
              </a>
            </div>
          </div>
        </div>

        <MobileHeader opened={menuOpened} onClick={handleMenuClick} />
      </div>
    </>
  );
};

export default Header;
