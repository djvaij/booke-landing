import Css from "./style.module.scss";

import React from "react";

import { PAGE_DEMO_PATH } from "utils/const";

const Banner = () => {
  return (
    <div className={Css.banner}>
      <div className="container">
        <div className={Css.container}>
          <div className={Css.left}>
            <img src="/images/fig-4.svg" />
          </div>
          <div className={Css.right}>
            <div className={Css.textLarge}>Start now and streamline your accounting workflow</div>
            <a href={PAGE_DEMO_PATH} className={`button ${Css.button}`}>Request Trial</a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Banner;
