/* eslint-disable no-magic-numbers */
import Css from "./style.module.scss";

import React, { useCallback, useState } from "react";
import classNames from "classnames";

import InViewport from "lib/common/InViewport";
import useMediaQuery from "hooks/useMediaQuery";

import Calculate from "./lib/Calculate";
import Columns from "./lib/Columns";
import Table from "./lib/Table/index";

const BASIC_FEATURES = [
  "Xero Integration",
  "QuickBooks Online Integration",
  "Client portal",
  "Reconciliation AI assistant",
  "Unlimited transactions",
  "Email support 9/5",
  "Automatic client notifications",
  "Unlimited client users",
  "Unlimited coworkers",
  "QuickBooks Desktop Integration",
  "Task management",
  "Encrypted files storage",
  "Documents workflow (OCR + AI)",
  "Email inbox for documents",
  "Dropbox & Gdrive import",
  "Bank statement processing",
  "White-label",
  "P&L smart analytics"
];

const PLANS = [
  { name: "Start", perUser: 5, level: 9 },
  { name: "Smart", perUser: 10, level: 12 },
  { name: "Premium", perUser: 20, level: Infinity }
];

const Pricing = () => {
  const matches = useMediaQuery("(max-width: 560px)");

  const [annual, setAnnual] = useState(true);

  const handleSwitcherClick = useCallback(() => {
    setAnnual((prev) => !prev);
  }, []);

  return (
    <div id="prices" className={Css.pricing}>
      <div className="container">
        <InViewport>
          {({ shownOnce }) => (
            <div className={`fadeIn ${!shownOnce && "notShown"}`}>
              <h1 className={Css.title}>Pricing</h1>
              <div className={classNames(Css.switcher, annual && Css.annual)} onClick={handleSwitcherClick}>
                <div className={Css.item}>Annually<span>-20%</span></div>
                <div className={Css.item}>Monthly</div>
              </div>
            </div>
          )}
        </InViewport>

        <InViewport>
          {({ shownOnce }) => (
            <div className={`fadeIn ${!shownOnce && "notShown"}`}>
              <Calculate plans={PLANS} annual={annual} />
            </div>
          )}
        </InViewport>

        <InViewport>
          {({ shownOnce }) => (
            <div className={`fadeIn ${!shownOnce && "notShown"}`}>
              <h1 className={Css.title}>Features</h1>

              {matches
                ? (<Columns plans={PLANS} />)
                : (<Table plans={PLANS} features={BASIC_FEATURES} />)}
            </div>
          )}
        </InViewport>
      </div>
    </div>
  );
};

export default Pricing;
