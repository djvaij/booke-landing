import Css from "./style.module.scss";

import React, { useCallback, useState } from "react";

import IconAvailable from "lib/icons/IconAvailable";
import IconCaretDown from "lib/icons/IconCaretDown";
import IconCaretUp from "lib/icons/IconCaretUp";

const SHORT_LIST_LENGTH = 4;

const FEATURES = [
  [
    "Client portal",
    "Xero Integration",
    "QuickBooks Online Integration",
    "Reconciliation AI assistant",
    "Unlimited transactions",
    "Email support 9/5",
    "Automatic client notifications",
    "Unlimited client users",
    "Unlimited coworkers"
  ],
  [
    (<strong key={0}>Includes Start Features</strong>),
    "QuickBooks Desktop Integration",
    "Task management",
    "Encrypted files storage"
  ],
  [
    (<strong key={0}>Includes Smart Features</strong>),
    "Documents workflow (OCR + AI)",
    "Bank statement processing",
    "Email inbox for documents",
    "Dropbox & Gdrive import",
    "White-label",
    "P&L smart analytics"
  ]
];

const Column = ({ plan, features }) => {
  const [showAll, setShowAll] = useState(false);

  const handleButtonClick = useCallback(() => {
    setShowAll((prev) => !prev);
  }, []);

  return (
    <div className={Css.column}>
      <div className={Css.title}>{plan.name}</div>
      <div className={Css.list}>
        {features.slice(0, showAll ? features.length : SHORT_LIST_LENGTH).map((feature) => (
          <div className={Css.item} key={feature}>
            <IconAvailable />
            <span>{feature}</span>
          </div>
        ))}
      </div>
      {(features.length > SHORT_LIST_LENGTH) && (
        <button className={`stroke block${showAll ? " secondary" : ""}`} onClick={handleButtonClick}>
          <div>
            {showAll
              ? (
                <>
                  <span>Hide features</span>
                  <IconCaretUp />
                </>
              )
              : (
                <>
                  <span>See all features</span>
                  <IconCaretDown />
                </>
              )}
          </div>
        </button>
      )}
    </div>
  );
};

const Columns = ({ plans = [] }) => {
  return (
    <div className={Css.columns}>
      {plans.map((plan, index) => (
        <Column key={plan.name} plan={plan} features={FEATURES[index]} />
      ))}
    </div>
  );
};

export default Columns;
