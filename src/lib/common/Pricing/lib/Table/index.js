import Css from "./style.module.scss";

import React from "react";

import IconAvailable from "lib/icons/IconAvailable";
import IconUnavailable from "lib/icons/IconUnavailable";

const Table = ({ plans, features }) => {
  return (
    <table className={Css.table}>
      <thead>
        <tr>
          <th>Key Feature</th>
          {plans.map(({ name }) => (<th key={name}>{name}</th>))}
        </tr>
      </thead>
      <tbody>
        {features.map((feature, index) => (
          <tr key={feature}>
            <td>{feature}</td>
            {plans.map(({ name, level }) => (<td key={name}>{level > index ? (<IconAvailable />) : (<IconUnavailable />)}</td>))}
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default Table;
