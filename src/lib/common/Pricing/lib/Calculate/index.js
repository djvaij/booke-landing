/* eslint-disable no-magic-numbers */
import Css from "./style.module.scss";

import React, { useCallback, useState } from "react";
import classNames from "classnames";

import { CALENDLY_WIDGET_URL } from "utils/const";
import Slider from "lib/common/Slider";

const USER_COUNTS = [
  [10, "10"],
  [50, "50"],
  [100, "100"],
  [200, "200"],
  [300, "300"],
  [400, "400"],
  [500, "500"],
  [600, "600"],
  [700, "700"],
  [800, "800+"]
];

const Calculate = ({ plans, annual }) => {
  const multiplier = annual ? 0.8 : 1;

  const [sliderValue, setSliderValue] = useState(1);

  const [selectedPlan, setSelectedPlan] = useState(plans[0].name);

  const handleSliderChange = useCallback((value) => {
    if (!USER_COUNTS[value]) return;
    setSliderValue(value);
  }, []);

  const handlePlanClick = useCallback((event) => {
    setSelectedPlan(event.currentTarget.dataset.plan);
  }, []);

  const total = Math.ceil(
    USER_COUNTS[sliderValue][0] * plans.find((plan) => plan.name === selectedPlan).perUser * multiplier
  );

  return (
    <div className={Css.calculate}>
      <div className={Css.variants}>
        <div className={Css.title}>Choose plan</div>
        <div className={Css.plans}>
          {plans.map(({ name, perUser }) => {
            return (
              <div
                key={name}
                className={classNames(Css.plan, selectedPlan === name && Css.active)}
                data-plan={name}
                onClick={handlePlanClick}>
                <div className={Css.name}>{name}</div>
                <div className={Css.price}>
                  <span>{`$${perUser * multiplier}`}</span> / client
                </div>
              </div>
            );
          })}
        </div>
        <div className={Css.title}>Choose number of clients</div>
        <div className={Css.slider}>
          <Slider
            min={0}
            max={USER_COUNTS.length - 1}
            value={sliderValue}
            label={`${USER_COUNTS[sliderValue][1]} clients`}
            onChange={handleSliderChange} />
        </div>
      </div>
      <div className={Css.result}>
        <div className={Css.content}>
          <div className={Css.value}>{`$${total}`}</div>
        </div>
        <div className={Css.payment}>
          <div>per clients/month</div>
          <div style={{ visibility: annual ? "visible" : "hidden" }}>billed annually</div>
        </div>
        <a href={CALENDLY_WIDGET_URL}>
          <button disabled={!total}>Choose this plan</button>
        </a>
      </div>
    </div>
  );
};

export default Calculate;
