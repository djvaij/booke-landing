import Css from "./style.module.scss";
import React from "react";
import classNames from "classnames";
import useIos from "hooks/useIos";
import useMediaQuery from "hooks/useMediaQuery";

const Feature = ({
  title,
  description,
  video = null,
  bgImage,
  bgPosition,
  className,
  reverse = false,
  customAnimationBlock = null
}) => {
  const tabletWidth = useMediaQuery("(min-width: 768px)");

  const { iosPlatform } = useIos();

  return (
    <div className={classNames(Css.container, { [Css.reverse]: reverse }, className)}>
      {!!customAnimationBlock && customAnimationBlock}
      {!!video && (
        <div className={Css.demo}>
          <div
            className={Css.bgImageWrapper}
            style={tabletWidth
              ? { top: bgPosition.top, left: bgPosition.left, right: "initial" }
              : { top: "-16px", left: "-16px", right: "-16px" }}>
            {bgImage}
          </div>
          {iosPlatform && (
            <img className={Css.video} src={video.mpegUrl} alt="Muted video" />
          )}
          {iosPlatform || (
            <video
              className={Css.video}
              autoPlay
              muted
              loop>
              <source src={video.webmUrl} type="video/webm" />
              <source src={video.mpegUrl} type="video/mp4" />
            </video>
          )}
        </div>)}
      <div className={Css.content}>
        <h3 className={Css.title}>{title}</h3>
        <p className={Css.description}>{description}</p>
      </div>
    </div>
  );
};

export default Feature;
