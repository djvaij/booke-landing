import Css from "./style.module.scss";

import React, { useCallback, useEffect, useRef } from "react";

import { PAGE_REQUEST_ACCESS_PATH } from "utils/const";

const Main = () => {
  const containerRef = useRef();

  const handleWindowResize = useCallback(() => {
    const width = window.innerWidth;

    const container = containerRef.current;

    // eslint-disable-next-line no-magic-numbers
    if (width > 1200 || width < 768) {
      container.style.height = "";
    } else {
      // eslint-disable-next-line no-magic-numbers
      container.style.height = `${width * 0.517}px`;
    }
  }, []);

  useEffect(() => {
    window.addEventListener("resize", handleWindowResize);

    handleWindowResize();

    return () => {
      window.removeEventListener("resize", handleWindowResize);
    };
  }, [handleWindowResize]);

  return (
    <div className={Css.main}>
      <div className={`container ${Css.container}`} ref={containerRef}>
        <div className="row">
          <div className={Css.left}>
            <div className={Css.textLarge}>
              <div><span>Where bookkeepers</span><br />get work done <img src="/images/lightning.png" /></div>
            </div>
            <div className={Css.textSmall}>
              Serve 5x more clients with same staff capacity.<br />
              Say goodbye to manual routine bookkeeping workflow.
            </div>
            <a href={PAGE_REQUEST_ACCESS_PATH} className={`button ${Css.button}`}>Start a bookkeeping</a>
          </div>
          <div className={Css.right}>
            <img src="/images/main-block-illustration.png" alt="Booke.ai" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Main;
