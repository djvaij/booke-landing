import Css from "./style.module.scss";

import React, { useMemo } from "react";

const DEFAULT_MAX_VALUE = 5;

const Icon = ({ selected }) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="none">
      <path
        d="M7.564.773a.5.5 0 0 1 .872 0l1.99 3.533a.5.5 0 0 0 .338.244l3.975.802a.5.5 0 0 1
        .27.829l-2.746 2.985a.5.5 0 0 0-.128.396l.465 4.029a.5.5 0 0 1-.704.512l-3.688-1.688a.5.5 0 0 0-.416 0l-3.688
        1.688a.5.5 0 0 1-.704-.512l.465-4.029a.5.5 0 0 0-.128-.396L.992 6.181a.5.5 0 0 1 .27-.829l3.974-.802a.5.5 0 0 0
        .337-.244L7.564.773Z" fill={selected ? "#FFBD14" : "#E4E4E4"} />
    </svg>
  );
};

const Rating = ({ max = DEFAULT_MAX_VALUE, value }) => {
  const items = useMemo(() => {
    return [...(new Array(max))].map((el, index) => ({ index, selected: value > index }));
  }, [max, value]);

  return (
    <div className={Css.rating}>
      {items.map(({ index, selected }) => (
        <div key={index} className={Css.item}>
          <Icon selected={selected} />
        </div>
      ))}
    </div>
  );
};

export default Rating;
