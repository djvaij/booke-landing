import Css from "./style.module.scss";

import React from "react";

import InViewport from "lib/common/InViewport";
import Rating from "./lib/Rating";

import ITEMS from "./mock.json";

const Testimonials = () => {
  return (
    <div className={Css.testimonials}>
      <InViewport>
        {({ shownOnce }) => (
          <>
            <div className={`container fadeInFromTop ${!shownOnce && "notShown"}`}>
              <h1>Impress your clients with simplicity</h1>
              <div className={Css.textSmall}>
                Outsource accountants waste up to 100 hours monthly to solve data entry issues for 100 clients.
                We made it unnecessary.
              </div>
            </div>
            <div className={`${Css.items} fadeInFromBottom ${!shownOnce && "notShown"}`}>
              {ITEMS.map((item, index) => {
                return (
                  // eslint-disable-next-line react/no-array-index-key
                  <div key={index} className={Css.item}>
                    <div className={Css.info}>
                      <div className={Css.image}><img src={item.image} /></div>
                      <div className={Css.title}>
                        <div className={Css.name}>{item.name}</div>
                        <div className={Css.position}>{item.title}</div>
                      </div>
                      <div className={Css.rating}>
                        <Rating value={item.rating} />
                      </div>
                    </div>
                    <div className={Css.comment}>{item.content}</div>
                  </div>
                );
              })}
            </div>
          </>
        )}
      </InViewport>
    </div>
  );
};

export default Testimonials;
