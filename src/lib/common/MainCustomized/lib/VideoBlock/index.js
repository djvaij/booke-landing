import Css from "./styles.module.scss";
import React from "react";
import classNames from "classnames";
import useIos from "hooks/useIos";

const VideoBlock = ({ className, webmUrl, mpegUrl }) => {
  const { iosPlatform } = useIos();

  if (iosPlatform) {
    return (
      <img
        width="623"
        height="388"
        className={classNames(Css.video, className)}
        src={mpegUrl}
        alt="Muted video" />
    );
  }

  return (
    <video
      width="623"
      height="388"
      className={classNames(Css.video, className)}
      autoPlay
      muted
      loop
      playsInline>
      <source src={webmUrl} type="video/webm" />
      <source src={mpegUrl} type="video/mp4" />
    </video>
  );
};

export default VideoBlock;
