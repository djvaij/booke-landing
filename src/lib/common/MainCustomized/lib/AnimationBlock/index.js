import React from "react";
import useCheckSafariBrowser from "hooks/useCheckSafariBrowser.js";
import useIos from "hooks/useIos";
import useWebmSupport from "hooks/useWebmSupport.js";

const AnimationBlock = ({ videoUrl, movUrl, pngUrl, className, alt, ...restProps }) => {
  const { webmSupported } = useWebmSupport();

  const { safariBrowser } = useCheckSafariBrowser();

  const { iosPlatform } = useIos();

  if (iosPlatform) {
    return (
      <img className={className} src={movUrl} alt="Muted video" {...restProps} />
    );
  }

  if (safariBrowser && movUrl) {
    return (
      <video className={className} autoPlay loop muted playsInline {...restProps}>
        <source src={movUrl} type='video/mp4; codecs="hvc1"' />
      </video>
    );
  }

  if (webmSupported) {
    return (
      <video className={className} autoPlay loop muted playsInline {...restProps}>
        <source src={movUrl} type='video/mp4; codecs="hvc1"' />
        <source src={videoUrl} type="video/webm" />
      </video>
    );
  }

  return (
    <img className={className} src={pngUrl} alt={alt} />
  );
};

export default AnimationBlock;
