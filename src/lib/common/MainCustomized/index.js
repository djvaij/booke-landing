import Css from "./style.module.scss";

import AnimationBlock from "./lib/AnimationBlock";
import React from "react";
import VideoBlock from "./lib/VideoBlock";
import classNames from "classnames";

import { PAGE_REQUEST_ACCESS_PATH } from "utils/const";

const MainCustomized = ({
  title,
  description,
  animationClass,
  animationResources = null,
  video = null,
  customAnimationBlock = null,
  rightClass,
  leftClass,
  containerClass
}) => {
  return (
    <div className={Css.main}>
      <div className={`container ${Css.container} ${containerClass}`}>
        <div className="row">
          <div className={classNames(Css.left, leftClass)}>
            <div className={Css.textLarge}>
              <div>{title}<img className={Css.lightning} src="/images/lightning-right.png" /></div>
            </div>
            <div className={Css.textSmall}>
              {description}
            </div>
            <div className={Css.buttonWrapper}>
              <a href={PAGE_REQUEST_ACCESS_PATH} className={`button ${Css.button}`}>Start a bookkeeping</a>
              <div className={Css.free}>
                {`Free forever
                No credit card`}
              </div>
            </div>
          </div>
          <div className={classNames(Css.right, rightClass)}>
            {animationResources && (
              <AnimationBlock
                videoUrl={animationResources.videoUrl}
                movUrl={animationResources.movUrl}
                pngUrl={animationResources.pngUrl}
                className={animationClass} />
            )}
            {video && <VideoBlock webmUrl={video.webmUrl} mpegUrl={video.mpegUrl} />}
            {customAnimationBlock && customAnimationBlock}
          </div>
        </div>
      </div>
    </div>
  );
};

export default MainCustomized;
