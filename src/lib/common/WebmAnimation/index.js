import React from "react";
import useCheckSafariBrowser from "hooks/useCheckSafariBrowser";
import useIos from "hooks/useIos";
import useWebmSupport from "hooks/useWebmSupport.js";

const WebmAnimation = ({ className, webmUrl, movUrl, pngUrl, width, height, alt, ...restProps }) => {
  const webmSupported = useWebmSupport();

  const { safariBrowser } = useCheckSafariBrowser();

  const { iosPlatform } = useIos();

  if (iosPlatform || safariBrowser) {
    return (
      <img
        className={className}
        src={movUrl}
        alt={alt}
        width={width}
        height={width}
        {...restProps} />
    );
  }

  if (safariBrowser && movUrl) {
    return (
      <video
        className={className}
        autoPlay
        loop
        muted
        playsInline
        width={width}
        height={height}
        {...restProps}>
        <source src={movUrl} type='video/mp4; codecs="hvc1"' />
      </video>
    );
  }

  if (webmSupported) {
    return (
      <video
        loop
        autoPlay
        muted
        playsInline
        className={className}
        width={width}
        height={height}>
        <source src={movUrl} type='video/mp4; codecs="hvc1"' />
        <source src={webmUrl} type="video/webm" />
      </video>
    );
  }

  return (
    <img className={className} src={pngUrl} alt={alt} />
  );
};

export default WebmAnimation;
