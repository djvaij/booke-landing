import Css from "./style.module.scss";

import React from "react";

import InViewport from "lib/common/InViewport";

const VideoFeedback = () => {
  return (
    <div className={Css.videoFeedback}>
      <InViewport>
        {({ shownOnce }) => (
          <>
            <div className={`container fadeInFromTop ${!shownOnce && "notShown"}`}>
              <h1>Impress your clients with simplicity</h1>
              <div className={Css.textSmall}>
                Outsource accountants waste up to 100 hours monthly to solve data entry issues for 100 clients.
                We made it unnecessary.
              </div>
            </div>
            <div className={`container fadeInFromBottom ${!shownOnce && "notShown"}`}>
              <div className={Css.video}>
                <iframe
                  src="https://player.vimeo.com/video/670734298?h=2edba1d3b1&title=0&byline=0&portrait=0"
                  frameBorder="0"
                  allowFullScreen />
              </div>
            </div>
          </>
        )}
      </InViewport>
    </div>
  );
};

export default VideoFeedback;
