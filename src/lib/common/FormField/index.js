import Css from "./style.module.scss";

import React from "react";

import { IconInvalid, IconValid } from "lib/icons";

const FormField = (props) => {
  const {
    icon,
    type = "text",
    name,
    value = "",
    error,
    placeholder,
    multiline,
    onChange,
    ...restProps
  } = props;

  const Component = multiline ? "textarea" : "input";

  return (
    <label className={Css.formField}>
      <Component
        type={multiline ? null : type}
        name={name}
        value={value}
        placeholder={placeholder}
        className={error ? Css.errorState : undefined}
        onChange={onChange}
        {...restProps} />
      <div className={Css.iconLabel}>{icon}</div>
      <div className={Css.iconValid}><IconValid /></div>
      <div className={Css.iconInvalid}><IconInvalid /></div>
    </label>
  );
};

export default FormField;
