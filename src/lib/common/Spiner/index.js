import Css from "./style.module.scss";

import React from "react";

const Spiner = () => {
  return (
    <div className={Css.spiner} />
  );
};

export default Spiner;
