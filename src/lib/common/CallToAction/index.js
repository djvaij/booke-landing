import Css from "./style.module.scss";

import React from "react";

import { CALENDLY_WIDGET_URL } from "utils/const";
import InViewport from "lib/common/InViewport";

const CallToAction = ({ disableAnimation = false }) => {
  return (
    <div className={Css.callToAction}>
      <div className="container">
        <InViewport>
          {({ shownOnce }) => (
            <div className={`${Css.container} ${disableAnimation ? "" : "fadeInZoomIn"} ${!shownOnce && "notShown"}`}>
              <div className={Css.left}>
                <img src="/images/fig-4.svg" />
              </div>
              <div className={Css.right}>
                <div className={Css.textLarge}>Start now and streamline your accounting workflow</div>
                <a href={CALENDLY_WIDGET_URL} className={`button ${Css.button}`}>Contact Sales</a>
              </div>
            </div>
          )}
        </InViewport>
      </div>
    </div>
  );
};

export default CallToAction;
