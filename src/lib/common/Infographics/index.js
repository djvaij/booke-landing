import Css from "./style.module.scss";

import React from "react";
import classNames from "classnames";

import { PAGE_DEMO_PATH } from "utils/const";
import InViewport from "lib/common/InViewport";

const Infographics = ({ quickBooksOnly }) => {
  return (
    <div className={Css.infographics}>
      <div className="container">
        <InViewport>
          {({ shownOnce, inViewport }) => (
            <div className={`row-m${(shownOnce || inViewport) ? " fadeInFromBottom" : ""}`}>
              <div className={Css.left}>
                <div className={Css.bgCircle} />
                <div className={Css.row}>
                  <div className={Css.blockA}>Synchronizes and classifies records</div>
                  <div className={Css.blockB}>
                    <div className={Css.textLarge}>1200+</div>
                    <div className={Css.textSmall}>Saved hours</div>
                    <div className={Css.image}>
                      <img src="/images/image-clock.svg" />
                    </div>
                  </div>
                </div>
                <div className={Css.row}>
                  <div className={classNames(Css.blockC, quickBooksOnly && Css.quickBooksOnly)}>
                    {quickBooksOnly
                      ? <img src="/images/qb-text-logo.svg" />
                      : <>
                        <img src="/images/xero-logo.svg" />
                        <img src="/images/qb-logo.svg" />
                      </>}
                  </div>
                  <div className={Css.blockD}>
                    Asks client to fill automatically
                  </div>
                </div>
              </div>
              <div className={Css.right}>
                <h1 className={Css.textMain}>All you dreamed of in one solution. And even more!</h1>
                <div className={Css.textSecondary}>
                  We can prove that Accountants have lots of free time with Booke
                </div>
                <a href={PAGE_DEMO_PATH} className={`button ${Css.button}`}>Try Booke</a>
              </div>
            </div>
          )}
        </InViewport>
        <InViewport>
          {({ shownOnce }) => (
            <div className={classNames(Css.secondBlock, "fadeInFromBottom", !shownOnce && "notShown")}>
              <h1 className={Css.textMain}>Auto features<br />you’ve been waiting for so long</h1>
              {quickBooksOnly ? <img src="/images/qb-scheme.svg" /> : <img src="/images/scheme.svg" />}
            </div>
          )}
        </InViewport>
      </div>
    </div>
  );
};

export default Infographics;
