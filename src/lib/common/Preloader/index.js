import Css from "./style.module.scss";

import React from "react";

const PreloaderDotted = ({ className, absolute, fixed }) => {
  return (
    <div
      className={[Css.preloaderDotted, absolute && Css.absolute, fixed && Css.fixed, className].filter(Boolean).join(" ")}>
      <div className={Css.animation}>
        <span />
        <span />
        <span />
      </div>
    </div>
  );
};

export default React.memo(PreloaderDotted);
