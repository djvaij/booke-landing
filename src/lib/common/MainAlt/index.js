import Css from "./style.module.scss";

import React from "react";

import { PAGE_DEMO_PATH } from "utils/const";

const MainAlt = ({ title, subtitle, description }) => {
  return (
    <div id="main" className={Css.main}>
      <div className={Css.wrapper}>
        <div className={`container row ${Css.container}`}>
          <div className={Css.left}>
            <h1 className={Css.title}>{title}</h1>
            <h2 className={Css.subtitle}>{subtitle}</h2>
            <p className={Css.description}>{description}</p>
            <a href={PAGE_DEMO_PATH} className={`button ${Css.button}`}>Watch а demo</a>
          </div>
          <div className={Css.right}>
            <img src="/images/main-block-illustration.svg" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default MainAlt;
