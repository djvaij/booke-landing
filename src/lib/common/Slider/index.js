import Css from "./style.module.scss";

import React, { useCallback, useEffect, useMemo, useRef, useState } from "react";
import classNames from "classnames";

const PERCENTS = 100;

const Slider = ({ value, disabled, min, max, label, onChange }) => {
  const trackRef = useRef();

  const [levelStyle, thumbStyle] = useMemo(() => {
    const sliderLeft = (value - min) / (max - min) * PERCENTS;

    return [{ right: `${PERCENTS - sliderLeft}%` }, { left: `${sliderLeft}%` }];
  }, [max, min, value]);

  const [tracking, setTracking] = useState(false);

  const moveThumb = useCallback((event) => {
    const { left: trackLeft, width: trackWidth } = trackRef.current.getBoundingClientRect();

    const clientX = (event.touches && event.touches[0]) ? event.touches[0].clientX : event.clientX;

    const val = Math.round(((max - min) * ((clientX - trackLeft) / trackWidth)));

    onChange(Math.min(Math.max(val, min), max));
  }, [max, min, onChange]);

  const handleMouseMove = useCallback((event) => {
    moveThumb(event);
  }, [moveThumb]);

  const handleMouseUp = useCallback((event) => {
    setTracking(false);
    moveThumb(event);
  }, [moveThumb]);

  const handleMouseLeave = useCallback(() => {
    setTracking(false);
  }, []);

  const handleMouseDown = useCallback(() => {
    if (disabled) return;
    setTracking(true);
  }, [disabled]);

  useEffect(() => {
    if (tracking) {
      window.addEventListener("mousemove", handleMouseMove);
      window.addEventListener("touchmove", handleMouseMove);
      window.addEventListener("mouseup", handleMouseUp);
      window.addEventListener("touchend", handleMouseLeave);
      window.addEventListener("mouseleave", handleMouseLeave);

      return () => {
        window.removeEventListener("mousemove", handleMouseMove);
        window.removeEventListener("touchmove", handleMouseMove);
        window.removeEventListener("mouseup", handleMouseUp);
        window.removeEventListener("touchend", handleMouseLeave);
        window.removeEventListener("mouseleave", handleMouseLeave);
      };
    }

    return () => {};
  }, [handleMouseLeave, handleMouseMove, handleMouseUp, tracking]);

  return (
    <div
      className={classNames(Css.slider, {
        [Css.disabled]: disabled,
        [Css.min]: value === min,
        [Css.max]: value === max
      })}
      onMouseDown={handleMouseDown}
      onTouchStart={handleMouseDown}>
      <div
        className={Css.track}
        ref={trackRef}>
        <div className={Css.scale}>
          {[...new Array(max - min)].map((item, index) => (<div key={String(index)} />))}
        </div>
        <div className={Css.level} style={levelStyle} />
        <div
          className={Css.thumb}
          style={thumbStyle}>
          {!!label && (<div className={Css.label}>{label}</div>)}
        </div>
      </div>
    </div>
  );
};

export default Slider;
