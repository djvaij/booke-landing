import Css from "./style.module.scss";

import React from "react";

import InViewport from "lib/common/InViewport";

const Features = () => {
  return (
    <div id="features" className={Css.features}>
      <div className="container">

        <InViewport>
          {({ shownOnce }) => (
            <div className={`row-m ${Css.firstBlock}`}>
              <div className={`col-1 ${Css.left} fadeInFromLeft ${!shownOnce && "notShown"}`}>
                <div className={Css.bg}>
                  <img src="/images/fig-1.svg" />
                </div>
              </div>
              <div className={`col-1 ${Css.right} fadeIn ${!shownOnce && "notShown"}`}>
                <div className={Css.textLarge}>Auto-categorization of transactions...</div>
                <div className={Css.textSmall}>
                  With 100% certainty we know how you categorize and therefore can do the majority of the work for you!
                </div>
              </div>
            </div>
          )}
        </InViewport>

        <InViewport>
          {({ shownOnce }) => (
            <div className={`row ${Css.secondBlock} fadeIn ${!shownOnce && "notShown"}`}>
              <div className={`col-1 ${Css.left}`}>
                <div className={Css.textLarge}>Better call your relatives, not clients</div>
                <div className={Css.textSmall}>
                  Because clients hate calls and lots of emails
                </div>
              </div>
              <div className={`col-1 ${Css.right} fadeInFromRight ${!shownOnce && "notShown"}`}>
                <img src="/images/fig-2.svg" />
              </div>
            </div>
          )}
        </InViewport>

        <InViewport>
          {({ shownOnce }) => (
            <div className={`row-m ${Css.thirdBlock}`}>
              <div className={`col-1 ${Css.left} fadeInFromLeft ${!shownOnce && "notShown"}`}>
                <img src="/images/fig-3.svg" />
              </div>
              <div className={`col-1 ${Css.right} fadeIn ${!shownOnce && "notShown"}`}>
                <div className={Css.textLarge}>Error-free P&amp;L</div>
                <div className={Css.textSmall}>
                  Human error accounts for 41% of inaccurate numbers in reporting.
                  We can help you reduce the errors by more than 90%!
                </div>
              </div>
            </div>
          )}
        </InViewport>

        <InViewport>
          {({ shownOnce }) => (
            <div className={`row ${Css.fourthBlock} fadeIn ${!shownOnce && "notShown"}`}>
              <div className={`col-1 ${Css.left}`}>
                <div className={Css.textLarge}>We know what you did last summer...</div>
                <div className={Css.textSmall}>
                  Booke logs all actions, so that you can free up your memory for more important things!
                </div>
              </div>
              <div className={`col-1 ${Css.right} fadeInFromRight ${!shownOnce && "notShown"}`}>
                <img src="/images/fig-5.svg" />
              </div>
            </div>
          )}
        </InViewport>
      </div>
    </div>
  );
};

export default Features;
