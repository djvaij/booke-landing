import { useEffect, useRef, useState } from "react";

const DEFAULT_TIME = 1500;

const fallback = (cb) => setTimeout(cb, 0);

const requestAnimationFrame = (cb) => {
  if (window.requestAnimationFrame) {
    window.requestAnimationFrame(cb);
  } else {
    fallback(cb);
  }
};

// eslint-disable-next-line
const defaultEasing = (time) => time;

const Counter = ({ from = 0, to, time = DEFAULT_TIME, animated, easing = defaultEasing }) => {
  const [value, setValue] = useState(from);

  const startTimeRef = useRef(null);

  useEffect(() => {
    if (!animated) return;

    if (!startTimeRef.current) {
      startTimeRef.current = Date.now();
    }

    requestAnimationFrame(() => {
      if (value < to) {
        const now = Date.now();

        const delta = to - from;

        const timeSpent = now - startTimeRef.current;

        setValue(delta * Math.min(1, easing(timeSpent / time)));
      }
    });
  }, [value, animated, to, from, easing, time]);

  return animated ? Math.floor(value) : to;
};

export default Counter;
