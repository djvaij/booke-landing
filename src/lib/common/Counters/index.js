import Css from "./style.module.scss";

import React from "react";

import Counter from "./lib/Counter";
import InViewport from "lib/common/InViewport";

const Counters = () => {
  return (
    <div className={Css.counters}>
      <div className="container">
        <div className={Css.container}>
          <div className={Css.bgCircle} />
          <InViewport className={Css.blockWrapper}>
            {({ shownOnce }) => (
              <div className={`${Css.block} ${Css.blockFirst} fadeInFromTop ${!shownOnce && "notShown"}`}>
                <div className={Css.textLarge}>
                  <Counter to={1200} time={1000} animated={shownOnce} />
                  <span>+</span>
                </div>
                <div className={Css.textSmall}>saved hours</div>
                <img src="/images/image-clock-lg.svg" />
              </div>
            )}
          </InViewport>
          <InViewport className={Css.blockWrapper}>
            {({ shownOnce }) => (
              <div className={`${Css.block} ${Css.blockSecond} fadeInFromTop ${!shownOnce && "notShown"}`}>
                <div className={Css.textLarge}>
                  <span>+</span>
                  <Counter to={70} time={1000} animated={shownOnce} />
                  <span>%</span>
                </div>
                <div className={Css.textSmall}>increase productivity</div>
                <img src="/images/image-rocket.svg" />
              </div>
            )}
          </InViewport>
          <InViewport className={Css.blockWrapper}>
            {({ shownOnce }) => (
              <div className={`${Css.block} ${Css.blockThird} fadeInFromTop ${!shownOnce && "notShown"}`}>
                <div className={Css.textLarge}>
                  <Counter to={1800} time={1000} animated={shownOnce} />
                  <span>+</span>
                </div>
                <div className={Css.textSmall}>used by business</div>
                <img src="/images/image-briefcase.svg" />
              </div>
            )}
          </InViewport>
          <InViewport className={Css.blockWrapper}>
            {({ shownOnce }) => (
              <div className={`${Css.block} ${Css.blockForth} fadeInFromTop ${!shownOnce && "notShown"}`}>
                <div className={Css.textLarge}>
                  <Counter to={400} time={1000} animated={shownOnce} />
                  <span>+</span>
                </div>
                <div className={Css.textSmall}>automated processes</div>
                <img src="/images/image-web-settings.svg" />
              </div>
            )}
          </InViewport>
        </div>
      </div>
    </div>
  );
};

export default Counters;
