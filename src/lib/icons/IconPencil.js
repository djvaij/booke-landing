import React from "react";

import Icon from "./Icon";

const IconPencil = () => (<Icon
  d="M6.75 15.187H3.375a.562.562 0 0 1-.563-.562v-3.142a.562.562 0 0 1 .165-.398l8.438-8.437a.562.562 0 0 1
  .795 0l3.142 3.142a.563.563 0 0 1 0 .795L6.75 15.187Zm0 0h8.438m-8.438 0-3.902-3.901M9.562 4.5 13.5 8.437" />);

export default IconPencil;
