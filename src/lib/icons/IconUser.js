import React from "react";

import Icon from "./Icon";

const IconUser = () => (<Icon
  d="M9 11.25a4.5 4.5 0 1 0 0-9 4.5 4.5 0 0 0 0 9Zm0 0a7.878
  7.878 0 0 0-6.821 3.937m6.82-3.937a7.877 7.877 0 0 1 6.822 3.937" />);

export default IconUser;
