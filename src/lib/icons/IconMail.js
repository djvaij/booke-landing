import React from "react";

import Icon from "./Icon";

const IconMail = () => (<Icon
  d="m2.25 6 5.918 3.945a1.5 1.5 0 0 0 1.664 0L15.75 6m-12 8.25h10.5a1.5
  1.5 0 0 0 1.5-1.5v-7.5a1.5 1.5 0 0 0-1.5-1.5H3.75a1.5 1.5 0 0 0-1.5 1.5v7.5a1.5 1.5 0 0 0 1.5 1.5Z" />);

export default IconMail;
