import React from "react";

import Icon from "./Icon";

const IconUnavailable = () => {
  return (
    <Icon viewBox="0 0 16 16" stroke="none">
      <circle cx="8" cy="8" r="8" fill="#D8DBDE" />
      <path
        d="m10.813 5.188-5.626 5.625m5.625 0L5.189 5.186"
        stroke="#fff"
        strokeWidth="1.6"
        strokeLinecap="round"
        strokeLinejoin="round" />
    </Icon>
  );
};

export default IconUnavailable;
