import React from "react";

import Icon from "./Icon";

const IconClose = () => (
  <Icon viewBox="0 0 24 24" d="m18.75 5.25-13.5 13.5M18.75 18.75 5.25 5.25" />
);

export default IconClose;
