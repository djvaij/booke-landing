#!/bin/bash

hornWebhookUrl=${HORN_WEBHOOK_URL:-$(grep HORN_WEBHOOK_URL .env | cut -d "=" -f 2-)}
releaseEnv=${ENV_TYPE:-$(grep ENV_TYPE .env | cut -d "=" -f 2-)}
hornMessage="{ \"text\" :\"🌎 *Booke LANDING PAGE ($releaseEnv)*\n\n🚀 Deployed\" }"
if [ $releaseEnv == "production" ]; then
  echo "===> Deployed. Calling horn webhook: $hornWebhookUrl"
  curl -H "Accept: application/json" -H "Content-type: application/json" -X POST -d "$hornMessage" $hornWebhookUrl
fi