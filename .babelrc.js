module.exports = {
  presets: [
    "@babel/preset-env",
    "@babel/preset-react",
    [
      "next/babel",
      {
        "preset-env": {
          "targets": "> 0.1%, not dead"
        }
      }
    ]
  ]
};
