# *
User-agent: *
Allow: /

# Host
Host: https://booke.ai/

# Sitemaps
Sitemap: https://booke.ai/sitemap.xml
Sitemap: https://booke.ai/sitemap-0.xml
