import { Html, Head, Main, NextScript } from "next/document";

import { AUTH_URL, CALENDLY_WIDGET_URL } from "utils/const";

const CSS_STRING = `
#preloader {
  position: fixed;
  z-index: 999;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  transform: translateZ(0);
  background-color: #fff;
}
#preloader .spiner {
  position: absolute;
  top: 50%;
  left: 50%;
  width: 8rem;
  height: 8rem;
  margin-top: -4rem;
  margin-left: -4rem;
  animation: rotate 1.2s infinite linear;
  border-width: 0.25rem;
  border-style: solid;
  border-radius: 50%;
  border-color: transparent #6686ac #6686ac transparent;
}
#preloader .spiner::before, #preloader .spiner::after {
  content: "";
  position: absolute;
  top: 50%;
  left: 50%;
  border-width: 0.25rem;
  border-style: solid;
  border-radius: 50%;
}
#preloader .spiner::before {
  width: 7.25rem;
  height: 7.25rem;
  margin-top: -3.625rem;
  margin-left: -3.625rem;
  animation: rotate 1s infinite linear;
  border-color: transparent transparent #1db173 #1db173;
}
#preloader .spiner::after {
  width: 6.5rem;
  height: 6.5rem;
  margin-top: -3.25rem;
  margin-left: -3.25rem;
  animation: rotate 1.5s infinite linear;
  border-color: #003166 #003166 transparent transparent;
}
`;

export default function Document() {
  return (
    <Html>
      <Head>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin />
        <link href="https://fonts.googleapis.com/css2?family=Urbanist:wght@300;400;500;600;700&display=swap" rel="stylesheet" />
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="192x192" href="/favicon-192x192.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />

        <link rel="prerender" href={AUTH_URL} />
        <link rel="prerender" href={CALENDLY_WIDGET_URL} />

        <style dangerouslySetInnerHTML={{ __html: CSS_STRING }} />
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}