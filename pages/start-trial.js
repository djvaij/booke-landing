import ExternalRedirect from "lib/common/ExternalRedirect";
import { SIGNUP_URL } from "utils/const";

const RedirectToSignup = () => <ExternalRedirect to={SIGNUP_URL} />

export default RedirectToSignup;
