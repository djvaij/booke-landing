import Storyblok from "lib/Storyblok";
import HubPost from "pages/HubPost";
import { REVALIDATE_INTERVAL } from "const";

export const getStaticProps  = async({ params, preview = null }) => {
  try {
    const { data } = await Storyblok.get(`cdn/stories/hub/${params.slug}`, { cv: Date.now() });
    const { data: datasourceResponse } = await Storyblok.get("cdn/datasource_entries?datasource=sections");

    const section = datasourceResponse.datasource_entries.find((item) => item.name === params.section);

    return {
      props: {
        preview,
        story: data ? data.story : null,
        section: section || {}
      },
      revalidate: REVALIDATE_INTERVAL
    };
  } catch (exception) {}

  return {
    props: {}
  };
};

export const getStaticPaths = async () => {
  const [
    { data: storiesResponse },
    { data: datasourceResponse },
  ] = await Promise.all([
    Storyblok.get("cdn/stories?starts_with=hub/", { cv: Date.now() }),
    Storyblok.get("cdn/datasource_entries?datasource=sections", { cv: Date.now() })
  ]);

  if (!datasourceResponse || !datasourceResponse) {
    return {};
  }

  const paths = [];

  datasourceResponse.datasource_entries.forEach((section) => {
    storiesResponse.stories
      .filter((story) => story.content.section === section.value)
      .forEach((story) => {
        paths.push({ params: { section: section.name, slug: story.slug } });
      });
  });

  return {
    paths,
    fallback: true
  };
}

export default HubPost;

