import Storyblok from "lib/Storyblok";
import Hub from "pages/Hub";
import { REVALIDATE_INTERVAL, POSTS_PER_PAGE } from "const";

export default Hub;

export async function getStaticProps({ preview = false, params }) {
  const { data: datasourceResponse } = await Storyblok.get("cdn/datasource_entries?datasource=sections", { cv: Date.now() });

  if (!datasourceResponse) {
    return { props: {} };
  }

  const section = datasourceResponse.datasource_entries.find((item) => item.name === params.section);

  const { data: storiesResponse } = await Storyblok.get("cdn/stories", {
    cv: Date.now(),
    per_page: POSTS_PER_PAGE,
    page: 1,
    starts_with: "hub/",
    filter_query: {
      section: {
        in: section ? section.value : ""
      }
    }
  });

  return {
    props: {
      stories: storiesResponse ? storiesResponse.stories : null,
      sections: datasourceResponse ? datasourceResponse.datasource_entries : null,
      preview,
    },
    revalidate: REVALIDATE_INTERVAL
  };
}

export const getStaticPaths = async () => {
  const { data: datasourceResponse } = await Storyblok.get("cdn/datasource_entries?datasource=sections", { cv: Date.now() });

  return {
    paths: datasourceResponse
      ? datasourceResponse.datasource_entries.map((section) => ({
        params: { section: section.name }
      }))
      : [],
    fallback: true
  };
}