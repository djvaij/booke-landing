import Storyblok from "lib/Storyblok";
import Hub from "pages/Hub";
import { REVALIDATE_INTERVAL, POSTS_PER_PAGE } from "const";

export default Hub;

export async function getStaticProps({ preview = false }) {
  const [
    { data: storiesResponse },
    { data: datasourceResponse },
  ] = await Promise.all([
    Storyblok.get("cdn/stories", {
      cv: Date.now(),
      per_page: POSTS_PER_PAGE,
      page: 1,
      starts_with: "hub/"
    }),
    Storyblok.get("cdn/datasource_entries?datasource=sections", { cv: Date.now() })
  ]);

  return {
    props: {
      stories: storiesResponse ? storiesResponse.stories : null,
      sections: datasourceResponse ? datasourceResponse.datasource_entries : null,
      preview,
    },
    revalidate: REVALIDATE_INTERVAL
  };
}