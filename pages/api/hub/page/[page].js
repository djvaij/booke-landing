import Storyblok from "lib/Storyblok";
import { POSTS_PER_PAGE } from "const";

export default async (req, res) => {
  const page = parseInt(req.query.page);

  if (isNaN(page)) {
    return res.status(200).json({});
  }

  const [{ data: storiesResponse }] = await Promise.all([
    Storyblok.get("cdn/stories", {
      cv: Date.now(),
      per_page: POSTS_PER_PAGE,
      page,
      starts_with: "hub/"
    })
  ]);

  res.status(200).json({
    stories: storiesResponse ? storiesResponse.stories : null
  });
};
