import Storyblok from "lib/Storyblok";
import { POSTS_PER_PAGE } from "const";

export default async (req, res) => {
  const [
    { data: storiesResponse },
    { data: datasourceResponse },
  ] = await Promise.all([
    Storyblok.get("cdn/stories", {
      cv: Date.now(),
      per_page: POSTS_PER_PAGE,
      page: 1,
      starts_with: "hub/"
    }),
    Storyblok.get("cdn/datasource_entries?datasource=sections", { cv: Date.now() })
  ]);

  res.status(200).json({
    stories: storiesResponse ? storiesResponse.stories : null,
    sections: datasourceResponse ? datasourceResponse.datasource_entries : null,
  });
};
