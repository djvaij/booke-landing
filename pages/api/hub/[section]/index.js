import Storyblok from "lib/Storyblok";
import { POSTS_PER_PAGE } from "const";

export default async (req, res) => {
  const { data: datasourceResponse } = await Storyblok.get("cdn/datasource_entries?datasource=sections", { cv: Date.now() });

  if (!datasourceResponse) {
    res.status(200).json({ stories: [] });
  }

  const section = datasourceResponse.datasource_entries.find((item) => item.name === req.query.section);

  const { data: storiesResponse } = await Storyblok.get("cdn/stories", {
    cv: Date.now(),
    per_page: POSTS_PER_PAGE,
    page: 1,
    starts_with: "hub/",
    filter_query: {
      section: {
        in: section ? section.value : ""
      }
    }
  });

  res.status(200).json({
    stories: storiesResponse ? storiesResponse.stories : null
  });
};
