import CustomerStory from "pages/CustomerStories/CustomerStory";

import { getStories, getStory } from "utils/stories";

export default CustomerStory;

export async function getStaticProps({ params }) {
  const story = getStory(params.slug);

  return {
    props: {
      story
    }
  };
}

export const getStaticPaths = async () => {
  const stories = getStories();

  return {
    paths: stories.map(({ slug }) => ({ params: { slug } })),
    fallback: true
  };
}