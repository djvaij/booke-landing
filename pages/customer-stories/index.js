import CustomerStories from "pages/CustomerStories";

import { getStories } from "utils/stories";

export default CustomerStories;

export async function getStaticProps() {
  const stories = getStories();

  return {
    props: {
      stories
    }
  };
}